{
    'name': 'Load Products on Scroll',
    'version': '1.0',
    'category': 'Website',
    'description': """
    This module will give you the possibility to load products on scroll in eCommerce.

    """,
    'author': "MP Technolabs",
    'website': "",
    'license': '',
    'depends': ['website_sale'],
    'data': ['views/templates.xml'],
    'installable': True,
    'active': False,
}
