# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'FlexiRetail',
    'version': '1.1',
    'category': 'Point of Sale',
    'summary': 'Complete Retail Solution',
    'description': """
This module allows user to use multiple functionality from pos interface.
""",
    'author': 'Acespritech Solutions Pvt. Ltd.',
    'website': 'http://www.acespritech.com',
    'depends': ['base', 'account', 'sale', 'point_of_sale', 'hr', 'hr_payroll','model_manager','hr_payroll_account', 'purchase','shoe_embassy_app','website'],
    "data": [
        'security/ir.model.access.csv',
        'views/cash_points_view.xml',
        'views/pos_cross_selling.xml',
        'views/account_view.xml',
        'views/product_view.xml',
        'views/pos_view.xml',
        'views/res_config_view.xml',
        'views/dynamic_prod_page_rpt.xml',
        'views/prod_small_lbl_rpt.xml',
        'views/stock_picking_view.xml',
        'wizard/generate_product_ean13_view.xml',
        'wizard/wizard_product_page_report_view.xml',
        'wizard/wizard_product_small_label_report.xml',
        'wizard/wizard_pos_sale_report_view.xml',
        'wizard/wizard_sales_details_view.xml',
        'wizard/wizard_pos_x_report.xml',
        'data/data.xml',
        'data/template.xml',
        'data/page_design_data.xml',
        'data/small_design_data.xml',
        'data/gift_voucher_sequence.xml',
        'data/gift_card_product.xml',
#         'data/account_journal.xml',
        'data/product.xml',
        'views/front_sales_report_template.xml',
        'views/front_sales_report_pdf_template.xml',
        'views/pos_sales_report_pdf_template.xml',
        'views/pos_sales_report_template.xml',
        'views/sales_details_pdf_template.xml',
        'views/sales_details_template.xml',
        'views/front_sales_thermal_report_template_xreport.xml',
        'views/front_sales_report_pdf_template_xreport.xml',
        'views/res_users_view.xml',
        # 'views/pos_coupon_security.xml',
        # 'views/pos_coupon_view.xml',
        'views/res_company_view.xml',
        'views/loyalty_view.xml',
        'views/res_partner_view.xml',
        # 'views/bonus_return_view.xml',
        'report.xml',
        'views/flexiretail.xml',
        'views/gift_card.xml',
        'views/wallet_management_view.xml',
        'views/pos_mirror_image_template.xml',
        'views/aspl_voucher_view.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml',
        'static/src/xml/reservation.xml',
    ],
#     'external_dependencies': {
#         'python': ['wand','libmagickwand-dev','imagemagick'],
#     },
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
