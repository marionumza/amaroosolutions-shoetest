# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd.
#     (<http://acespritech.com>).
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

# from openerp.osv import osv,fields
from openerp import models, fields, api,tools,_
from bsddb.dbtables import _columns
from openerp.exceptions import UserError,Warning


class StockChangeProductQty(models.TransientModel):
    _inherit = 'stock.change.product.qty'
    
    @api.multi
    def change_product_qty(self):
        """ Changes the Product Quantity by making a Physical Inventory. """
        Inventory = self.env['stock.inventory']
        for wizard in self:
            product = wizard.product_id.with_context(location=wizard.location_id.id, lot_id=wizard.lot_id.id)
            line_data = wizard._prepare_inventory_line()

            if wizard.product_id.id and wizard.lot_id.id:
                inventory_filter = 'none'
            elif wizard.product_id.id:
                inventory_filter = 'product'
            else:
                inventory_filter = 'none'
            inventory = Inventory.create({
                'name': _('INV: %s') % tools.ustr(wizard.product_id.name),
                'filter': inventory_filter,
                'product_id': wizard.product_id.id,
                'location_id': wizard.location_id.id,
                'lot_id': wizard.lot_id.id,
                'line_ids': [(0, 0, line_data)],
                'real_qty':self.new_quantity
            })
            inventory.action_done()
        return {'type': 'ir.actions.act_window_close'}


class StockInventoryWarning(models.TransientModel):
    _name = 'stock.inventory.warning'
    
    @api.one
    def change_product_qty(self):
        pass

class Inventory(models.Model):
    _inherit = "stock.inventory"
    
    name = fields.Char('Inventory Reference', required=False, readonly=False,states={'draft': [('readonly', False)],'confirm': [('readonly', False)]})
    location_id = fields.Many2one(
        'stock.location', 'Inventoried Location',
        readonly=True, required=True,
        states={'draft': [('readonly', False)],'confirm': [('readonly', False)]},default=False)
    is_clear_stock = fields.Boolean('Is Clear Stock')
    
    @api.multi
    def clear_stock(self):
        if self.location_id.name == 'Stock':
            self.is_clear_stock = True
            inventory_line_ids = self.env['stock.inventory.line'].search([('location_id','=',self.location_id.id),
                                                                          ('inventory_id','=',self.id)
                                                                          ])
            for line_id in inventory_line_ids:
                line_id.product_qty = 0.0
        self.line_ids.filtered(lambda l: l.id not in inventory_line_ids.ids).unlink()
        
        return True
    @api.multi
    def action_done(self):
        for line in self.line_ids:
            if line.product_qty >= 9999999:
                msg = _('A quantity above 9999999 has been added could you verify and correct before validating.')
                raise UserError(msg)
        
        theoretical_qty_sum_before = sum([line.theoretical_qty for line in self.line_ids])
        theoretical_qty_sum_after = sum([line_values['theoretical_qty'] for line_values in self._get_inventory_lines_values()])
        inventory_line_obj = self.env['stock.inventory.line']
        if theoretical_qty_sum_before != theoretical_qty_sum_after and not self.is_clear_stock:
            inventory_line_vals = []
            for line_values in self._get_inventory_lines_values():
                line_id = inventory_line_obj.search([('product_id','=',line_values['product_id']),('inventory_id','=',self.id),('location_id','=',line_values['location_id'])],limit=1)
                if line_id:
                    line_values.update({'product_qty':line_id.product_qty})
                    line_id.unlink()
                else:
                    line_values.update({'product_qty':0})
                    line_id.unlink()
                inventory_line_vals.append((0, 0, line_values))
                
            self.line_ids = inventory_line_vals
            self.state = 'confirm'
            return {
                'view_id': self.env.ref('flexiretail.stock_inventory_warning_qty_missing').id,
                'view_type': 'form',
                "view_mode": 'form',
                'res_model': 'stock.inventory.warning',
                'type': 'ir.actions.act_window',
                'target': 'new'
            }
        if self.real_qty != sum([line.product_qty for line in self.line_ids]):
            self.mapped('line_ids').write({'product_qty': 0})
            self.state = 'confirm'
            return {
                'view_id': self.env.ref('flexiretail.stock_inventory_warning').id,
                'view_type': 'form',
                "view_mode": 'form',
                'res_model': 'stock.inventory.warning',
                'type': 'ir.actions.act_window',
                'target': 'new'
            }
        return super(Inventory, self).action_done()
    
    @api.onchange('name')
    def onchange_name(self):
        if self.name:
            location_id = self.env['stock.location'].search([('barcode','=',self.name)])
            if location_id:
                self.location_id = location_id.id
                for inventory in self:
                    if (inventory.filter != 'partial') and not inventory.line_ids:
                        self.line_ids = [(0, 0, line_values) for line_values in inventory._get_inventory_lines_values()]
                        self.date = fields.Datetime.now() 
                        self.state = 'confirm'
                        self.action_reset_product_qty()
#                     inventory.write(vals)
    real_qty = fields.Float(string='Real Quantity')
    
class stock_location(models.Model):
    _inherit = "stock.location"
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('barcode', operator, name)]
        picks = self.search(domain + args, limit=limit)
        return picks.name_get()

    cashier_ids = fields.Many2many('res.users','location_partner_rel','location_id','partner_id', string='Cashier')
    procurement_id = fields.Many2one('procurement.group',string='Procurement Group')

class stock_warehouse(models.Model):
    _inherit = 'stock.warehouse'
    
    @api.model
    def disp_prod_location_stock(self, product_id, shop_id,pre_order_stock_id):
        pre_order_stock_qty = 0
        shop_qty = 0
        total_qty = 0
        quant_obj = self.env['stock.quant']
        
        if pre_order_stock_id:
            loc_ids1 = self.env['stock.location'].search([('location_id','child_of',[pre_order_stock_id])])
            stock_quant_ids1 = quant_obj.search([('location_id','in',[loc_id.id for loc_id in loc_ids1]), ('product_id', '=', product_id)])
            for stock_quant_id1 in stock_quant_ids1:
                pre_order_stock_qty = stock_quant_id1.qty
        
        product_obj = self.env['product.product'].search([('id','=',product_id)])
        for warehouse_id in self.search([]):
            product_qty = 0.0
            ware_record = warehouse_id
            location_id = ware_record.lot_stock_id.id
            loc_ids = self.env['stock.location'].search([('location_id','child_of',[location_id])])
            stock_quant_ids = quant_obj.search([('location_id','in',[loc_id.id for loc_id in loc_ids]), ('product_id', '=', product_id)])
            for stock_quant_id in stock_quant_ids:
                product_qty += stock_quant_id.qty
            if ware_record.name == 'Main Warehouse':
                main_qty = product_qty
            total_qty += product_qty
            if location_id == shop_id:
                shop_qty = product_qty
        return shop_qty, total_qty, main_qty,pre_order_stock_qty
    
    @api.model
    def disp_prod_stock(self, product_id, shop_id):
        stock_line = []
        total_qty = 0
        shop_qty = 0
        quant_obj = self.env['stock.quant']
        for warehouse_id in self.search([]):
            product_qty = 0.0
            ware_record = warehouse_id
            location_id = ware_record.lot_stock_id.id
            if shop_id:
                loc_ids1 = self.env['stock.location'].search([('location_id','child_of',[shop_id])])
                stock_quant_ids1 = quant_obj.search([('location_id','in',[loc_id.id for loc_id in loc_ids1]), ('product_id', '=', product_id)])
                for stock_quant_id1 in stock_quant_ids1:
                    shop_qty = stock_quant_id1.qty

            loc_ids = self.env['stock.location'].search([('location_id','child_of',[location_id])])
            stock_quant_ids = quant_obj.search([('location_id','in',[loc_id.id for loc_id in loc_ids]), ('product_id', '=', product_id)])
            for stock_quant_id in stock_quant_ids:
                product_qty += stock_quant_id.qty
            stock_line.append([ware_record.name, product_qty, ware_record.id, ware_record.lot_stock_id.sequence])
            total_qty += product_qty
            
        return stock_line, total_qty, shop_qty

# class stock_production_lot(models.Model):
#     _inherit = 'stock.production.lot'

#     @api.one
#     def compute_remaining_qty(self, name, arg): 
#         res = {}
#         for each_lot in self.browse():
#             prod_qty = self.calc_prod_qty(attr=[('location_id.usage','=','internal')])
#             # each_lot.qty_remaining = prod_qty[each_lot.id]
#             res[each_lot.id] = prod_qty[each_lot.id]
#         return res
#     
#     qty_remaining = fields.Float(compute='compute_remaining_qty', string='Remaining Qty')
#     
#     def calc_prod_qty(self,attr=[]):
# #         context = dict(context or {})
#         res = {}
#         quant_obj = self.env['stock.quant']
#         uid_company_id = self.env['res.users'].browse(self.uid).company_id.id
# 
#         for each_lot in self.browse():
#             lot_qty = 0
#             lot_attr = attr[:]
#             lot_attr.append(('lot_id','=',each_lot.id))
#             required_quants = quant_obj.search(lot_attr)
#             for quant in quant_obj.browse(required_quants):
# #                 context.pop('force_company', None)
#                 if quant.company_id.id != uid_company_id:
#                     #if the company of the quant is different than the current user company, force the company in the context
#                     #then re-do a browse to read the property fields for the good company.
# #                     context['force_company'] = quant.company_id.id
#                     quant = self.with_context({'force_company' : quant.company_id.id}).browse(quant.id)
#                 lot_qty += quant.qty
#             res.update({each_lot.id:lot_qty})
#         return res
# 
#     def search(self, args, offset=0, limit=None, order=None, count=False):
#         ret_val = super(stock_production_lot,self).search(args,offset=offset, limit=limit, order=order,
#                                                       count=count)
#         if self._context.get('remaining_qty_only')  and self._context.get('pos_location_id'):
#             updated_vals = []
#             for eachlot in self.browse(ret_val):
#                 if eachlot.get_unused_lots(eachlot, self._context.get('pos_location_id')):
#                     updated_vals.append(eachlot.id)
#             return updated_vals
#         return ret_val
# 
#     def get_unused_lots(self, eachlot, location_id):
#         '''
#            To check the Quantity of products left in this lot
#         '''
#         internal_qty = 0
#         if not eachlot.quant_ids:
#         # This means this lot is created directly from odoo, so we can use this lot
#             return True
#         for quant in eachlot.quant_ids:
#         # here we check for multipple cases,
#         # 1. internal quant with +ve value, external with -ve value
#             if quant.location_id and (quant.location_id.id == location_id) and quant.location_id.usage and quant.location_id.usage == "internal":
#                 internal_qty += quant.qty
#         if internal_qty and internal_qty > 0:
#             return True
#         return False

class stock_picking(models.Model):
    _inherit = "stock.picking"
    
    delivery_count = fields.Integer(string='Delivery Orders', compute='_compute_picking_ids')
    warehouse_name_id = fields.Many2one('stock.warehouse',compute='get_warehouse_name',string='Dispatch Location')
    
    @api.multi
    @api.depends('move_lines')
    def get_warehouse_name(self):
        for picking in self:
            for move in picking.move_lines:
                if move.procurement_id and move.procurement_id.route_ids:
                    for route in move.procurement_id.route_ids:
                        picking.warehouse_name_id = route.procurement_warehouse_id and route.procurement_warehouse_id.id or False
                else:
                    picking.warehouse_name_id = move.procurement_id.warehouse_id and move.procurement_id.warehouse_id.id or False        
    @api.multi
    def action_view_delivery(self):
        action = self.env.ref('stock.action_picking_tree_all').read()[0]
        action['domain'] = [('group_id', '=', self.group_id and self.group_id.id or False)]
        return action    
    
    @api.multi
    def _compute_picking_ids(self):
        for order in self:
            picking_ids = self.env['stock.picking'].search([('group_id', '=', order.group_id and order.group_id.id or False)])
            order.delivery_count = len(picking_ids)
            
    def do_detailed_internal_transfer_for_preorder(self, vals):
        move_lines = []
        warehouse_id = self.env['stock.warehouse'].search([('lot_stock_id','=',vals.get('data').get('picking_type_id'))],limit=1) 
        if vals and vals.get('data'):
            for move_line in vals.get('data').get('moveLines'):
                move_lines.append((0,0,move_line))
            picking_vals = {'location_id': vals.get('data').get('location_src_id'),
                            'state':'draft', 
                            'move_lines': move_lines,
                            'location_dest_id': vals.get('data').get('location_dest_id'),
                            'picking_type_id': warehouse_id and warehouse_id.int_type_id and warehouse_id.int_type_id.id or False}
            picking_id = self.create(picking_vals)
            if picking_id:
                if vals.get('data').get('state') == 'confirmed':
                    picking_id.action_confirm()
                if vals.get('data').get('state') == 'done':
                    picking_id.action_confirm()
                    picking_id.force_assign()
                    picking_id.do_new_transfer()
                    stock_transfer_id = self.env['stock.immediate.transfer'].search([('pick_id', '=', picking_id.id)], limit=1)
                    if stock_transfer_id:
                        stock_transfer_id.process()
        return [picking_id.id,picking_id.name]
    
    def do_detailed_internal_transfer(self, vals):
        move_lines = [] 
        if vals and vals.get('data'):
            for move_line in vals.get('data').get('moveLines'):
                move_lines.append((0,0,move_line))
            picking_vals = {'location_id': vals.get('data').get('location_src_id'),
                            'state':'draft', 
                            'move_lines': move_lines,
                            'location_dest_id': vals.get('data').get('location_dest_id'),
                            'picking_type_id': vals.get('data').get('picking_type_id')}
            picking_id = self.create(picking_vals)
            if picking_id:
                if vals.get('data').get('state') == 'confirmed':
                    picking_id.action_confirm()
                if vals.get('data').get('state') == 'done':
                    picking_id.action_confirm()
                    picking_id.force_assign()
                    picking_id.do_new_transfer()
                    stock_transfer_id = self.env['stock.immediate.transfer'].search([('pick_id', '=', picking_id.id)], limit=1)
                    if stock_transfer_id:
                        stock_transfer_id.process()
        return [picking_id.id,picking_id.name]

class stock_production_lot(models.Model):
    _inherit = 'stock.production.lot'
    
    remaining_qty = fields.Float("Remaining Qty", compute="_compute_remaining_qty")
    
    def _compute_remaining_qty(self):
        for each in self:
            each.remaining_qty = 0
            for quant_id in each.quant_ids:
                if quant_id and quant_id.location_id and quant_id.location_id.usage == 'internal':
                    each.remaining_qty += quant_id.qty
        return

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
