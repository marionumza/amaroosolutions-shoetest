# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# 
# from openerp.osv import osv, fields
from openerp import models, fields, api, _
from datetime import timedelta
from datetime import datetime
import time

class bonus_return(models.Model):
    _name = "bonus.return"

    def _get_cust(self):
        users_obj = self.env['res.users'].browse(self._uid)
        company_id = users_obj.company_id.id
        return company_id

    company_id = fields.Many2one('res.company', 'Company', readonly=True, default=_get_cust) 
    name = fields.Char('Description', size=128)
    date_create = fields.Date('Issue Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    date_expire = fields.Date('Expiry Date')
    date_expire_num = fields.Integer()
    bonus_amount = fields.Float('Amount')
    bonus_remaining_amt = fields.Float('Remaining Amount', readonly=True)
    bonus_history = fields.One2many('pos.bonus.history', 'bonus_return_id', string='History Lines', readonly=True)

    def create(self, vals):
        if context is None:
            context = {}
        date_create = time.strftime("%Y:%m:%d")
        date_formatted_create = datetime.strptime(date_create , '%Y:%m:%d')
        if vals.get('date_expire_num') > 0:
            date_expire = date_formatted_create + timedelta(days=int(vals.get('date_expire_num')))
        else:
            date_expire = 0
        vals.update({'date_expire' : date_expire})
        res = super(bonus_return, self).create(vals)
        return res

bonus_return()

class pos_bonus_history(models.Model):
    _name = "pos.bonus.history"

    name = fields.Char('Bonus Serial', size=264)
    used_amount = fields.Float('Used Amount')
    used_date = fields.Date('Used Date', default=lambda *a: time.strftime('%Y-%m-%d'))
    bonus_return_id = fields.Many2one('bonus.return', string='Bonus Coupon')
    pos_order = fields.Char('POS Order')


pos_bonus_history()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: