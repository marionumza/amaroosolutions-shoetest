# -*- encoding: utf-8 -*-
##############################################################################

# from openerp.osv import osv,fields
from openerp import models, fields, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime, timedelta
import random

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    def check_order_availibity(self, order, warehouse_list):
        for warehouse_ids in warehouse_list:
            available_list = {warehouse_id : 0.0 for warehouse_id in warehouse_list[1]}
            for w_id in warehouse_ids:
                is_available = True
                for OrderLine in order.order_line:
                    if OrderLine.product_id.type == 'product':
                        shop_qty = 0.0
                        loc_ids1 = self.env['stock.location'].search([('location_id','child_of',[w_id.lot_stock_id.id])])
                        stock_quant_ids1 = self.env['stock.quant'].search([('location_id','in',[loc_id.id for loc_id in loc_ids1]), ('product_id', '=', OrderLine.product_id.id)])
                        for stock_quant_id1 in stock_quant_ids1:
                            shop_qty += stock_quant_id1.qty
                        if shop_qty < OrderLine.product_uom_qty:
                            is_available = False
                            if available_list.has_key(w_id) : available_list.pop(w_id)
                        else:
                            if available_list.has_key(w_id):
                               available_list[w_id] += shop_qty 
                if is_available and warehouse_list[0][0] == w_id:
                    return w_id
            if len(warehouse_ids) > 1 and  available_list:
                available_list_inverse = [(value, key) for key, value in available_list.items()]
                most_qty_warehouse = max(available_list_inverse)[1] 
                most_qty = available_list[most_qty_warehouse]
                duplicate_qty_list = []
                for a_key,a_value in available_list.iteritems():
                    if a_value == most_qty:
                        duplicate_qty_list.append(a_key)
                random.shuffle(duplicate_qty_list,random.random)
                return duplicate_qty_list[0]
        return False   

    @api.multi
    def action_confirm(self):
        for order in self:
            order.state = 'sale'
            warehouse_obj = self.env['stock.warehouse']
            change_line_data = False
            order.confirmation_date = fields.Datetime.now()
            if self.env.context.get('send_email'):
                self.force_quotation_send()
            main_warehouse_id = warehouse_obj.search([('name','=','Main Warehouse')],limit=1)
            qty_a_w = self.check_order_availibity(order,[[main_warehouse_id],warehouse_obj.search([('name','!=','Main Warehouse')])])
            if qty_a_w:
                order.warehouse_id = qty_a_w.id
                order.order_line._action_procurement_create()
            else:
                order.order_line.with_context(check_qty=True)._action_procurement_create()
        if self.env['ir.values'].get_default('sale.config.settings', 'auto_done_setting'):
            self.action_done()
        return True
    
class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"    
    
    @api.multi
    def _action_procurement_create(self):
        """
        Create procurements based on quantity ordered. If the quantity is increased, new
        procurements are created. If the quantity is decreased, no automated action is taken.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        new_procs = self.env['procurement.order']  # Empty recordset
        for line in self:
            if line.state != 'sale' or not line.product_id._need_procurement():
                continue
            qty = 0.0
            for proc in line.procurement_ids:
                qty += proc.product_qty
            if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
                continue

            if not line.order_id.procurement_group_id:
                vals = line.order_id._prepare_procurement_group()
                line.order_id.procurement_group_id = self.env["procurement.group"].create(vals)
            
            if line._context.get('check_qty'):
                days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
                delivery_chart_obj = self.env['delivery.chart']
                StockWarehouse = self.env['stock.warehouse']
                StockLocation = self.env['stock.location']
                p_location_id = StockLocation.search([('name','=','Customers')],limit=1) or False
                main_warehouse_id = StockWarehouse.search([('name','=','Main Warehouse')])
                delivery_chart_id = delivery_chart_obj.search([('day_of_week','=',days[datetime.now().weekday()]),('warehouse_group_id','=',main_warehouse_id.warehouse_group_id.id),('order_type','=','ho')])
                data = StockWarehouse.disp_prod_stock(line.product_id.id,main_warehouse_id.lot_stock_id.id)
                Delivery_chart = {}
                for dc in delivery_chart_id.delivery_chart_line_id:
                    if Delivery_chart.has_key(dc.delivery_days):
                        Delivery_chart[dc.delivery_days] = Delivery_chart[dc.delivery_days] + [w_id.name for w_id in dc.warehouse_group_id.warehouse_ids] 
                    else:
                        Delivery_chart.update({dc.delivery_days:[w_id.name for w_id in dc.warehouse_group_id.warehouse_ids]})
                proc_data = []
                r_qty = line.product_uom_qty
                for w_group in sorted(Delivery_chart.keys()):
                    w_data = [[ w for w in data[0] if w[0] in Delivery_chart[w_group]]]
                    proc_data = self.env['pos.order'].create_proc_data(w_data,r_qty,proc_data)
                    if isinstance(proc_data[-1],float):
                        r_qty = proc_data[-1]
                        del proc_data[-1]
                        [pc.append(w_group) for pc in proc_data]
                    else:
                        [pc.append(w_group) for pc in proc_data]
                        break
#                     proc_data = self.create_proc_data(data,line.qty,[])
                for pro in proc_data:
                    route_id = self.env['stock.location.route'].search([('location_ids','in',p_location_id.id),('procurement_warehouse_id','=',main_warehouse_id.id),('preferred_destination_warehouse_id','=',pro[2])])
                    r_route_id = route_id[0] if len(route_id) > 0 else False
                    if route_id and pro[1] > 0:
                        procurement_order_values = {
                                                'product_id':line.product_id.id,
                                                'product_uom':line.product_id.uom_id.id,
                                                'warehouse_id': main_warehouse_id.id,
                                                'location_id': p_location_id.id,
                                                'product_qty': pro[1],
                                                'route_ids': [(6,0,[r_route_id.id])],
                                                'name': line.order_id.name,
                                                'origin': line.order_id.name,
                                                'group_id': line.order_id.procurement_group_id.id,
                                                'partner_dest_id': line.order_id.partner_id and line.order_id.partner_id.id or False,
                                                'date_planned':(datetime.now() + timedelta(days=pro[-1]))
                                               }
                        procurement_id = self.env["procurement.order"].with_context(procurement_autorun_defer=True).create(procurement_order_values)
                        procurement_id.message_post_with_view('mail.message_origin_link',values={'self': procurement_id, 'origin': line.order_id},
                                                                                            subtype_id=self.env.ref('mail.mt_note').id)
                        new_procs += procurement_id
                continue
            
            vals = line._prepare_order_line_procurement(group_id=line.order_id.procurement_group_id.id)
            vals['product_qty'] = line.product_uom_qty - qty
            new_proc = self.env["procurement.order"].with_context(procurement_autorun_defer=True).create(vals)
            new_proc.message_post_with_view('mail.message_origin_link',
                values={'self': new_proc, 'origin': line.order_id},
                subtype_id=self.env.ref('mail.mt_note').id)
            new_procs += new_proc
        new_procs.run()
        return new_procs
    
#     @api.multi
#     def _action_procurement_create(self):
#         res = super(SaleOrderLine, self)._action_procurement_create()
#         orders = list(set(x.order_id for x in self))
#         for order in orders:
#             reassign = order.picking_ids.filtered(lambda x: x.state=='confirmed' or ((x.state in ['partially_available', 'waiting']) and not x.printed))
#             if reassign:
#                 reassign.do_unreserve()
#                 reassign.action_assign()
#         return res