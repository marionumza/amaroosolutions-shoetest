# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api, _
from datetime import datetime


class wallet_management(models.Model):
    _name = 'wallet.management'

    customer_id = fields.Many2one('res.partner', string='Customers')
    order_id = fields.Many2one('pos.order', string='Order')
    type = fields.Selection([('change','Change'),('return','Return')],string="Type")
    debit = fields.Float("Debit")
    credit = fields.Float("Credit")
    cashier_id = fields.Many2one('res.users',string='Cashier')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: