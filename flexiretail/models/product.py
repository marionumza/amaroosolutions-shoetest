# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from datetime import datetime


class product_template(models.Model):
    _inherit = "product.template"
    
#     @api.model
#     def get_product_by_brand(self, query):
#         product_list = []
#         product_template_search = self.search([('brand_id.name','ilike',query)])
#         if product_template_search:
#             for template_id in product_template_search:
#                 product_ids = self.env['product.product'].search([('product_tmpl_id','=',template_id.id)])
#                 if product_ids:
#                     for product in product_ids:
#                         product_list.append(product.id)
#         return product_list 
    available_in_pos = fields.Boolean(string='Available in the Point of Sale', compute='_compute_quantities',search='_search_qty_available_product',help='Check if you want this product to appear in the Point of Sale')
    
    def _search_qty_available_product(self, operator, value):
        search_domain = self._search_qty_available('>', 0)
        if search_domain:
            return ['|',('type', '=', 'service')]+search_domain
        return search_domain
    
    def _compute_quantities(self):
        res = self._compute_quantities_dict()
        for template in self:
            if template.type != 'service':
                template.qty_available = res[template.id]['qty_available']
                if template.qty_available > 0 and template.available_in_pos == False:
                    template.available_in_pos = True
                elif template.qty_available <= 0 and template.available_in_pos == True:
                    template.available_in_pos = False
                template.virtual_available = res[template.id]['virtual_available']
                template.incoming_qty = res[template.id]['incoming_qty']
                template.outgoing_qty = res[template.id]['outgoing_qty']
            else:
                template.available_in_pos = True
                
            
    @api.model
    def create(self, vals):
        res = super(product_template, self).create(vals)
        if res:
            if not vals.get('barcode') and self.env['sale.config.settings'].search([], limit=1, order="id desc").gen_ean13:
                barcode_str = self.env['barcode.nomenclature'].sanitize_ean("%s%s" % (res.id, datetime.now().strftime("%d%m%y%H%M")))
                res.write({'barcode' : barcode_str})
        return res
    
    is_packaging = fields.Boolean("Is Packaging")
    is_delivery = fields.Boolean("Is Delivery")
    can_not_return = fields.Boolean('Can not Return')

    @api.model
    def create_from_ui(self, product):
        if product.get('image'):
            product['image'] = product['image'].split(',')[1]
        id = product.get('id')
        if id:
            product_tmpl_id = self.env['product.product'].browse(id).product_tmpl_id
            if product_tmpl_id:
                product_tmpl_id.write(product)
        else:
            id = self.env['product.product'].create(product).id
        return id

    def assign_lot(self, data):
        if data.get('serial_no') :
           prod_id = self.env['product.product'].search([('product_tmpl_id','=',self.id)])
           if prod_id and data.get('location_id'):
               lot_id = self.env['stock.production.lot'].create({'product_id':prod_id.id,'name':data.get('serial_no')})
               update_qty_id = self.env['stock.change.product.qty'].create({'product_id' : prod_id.id,
                                                                            'location_id' : data.get('location_id'),
                                                                            'new_quantity':data.get('qty'),'lot_id' : lot_id.id})
               update_qty_id.change_product_qty()
               return True
           return False


class product_product(models.Model):
    _inherit = "product.product"
    
    @api.model
    def create(self, vals):
        res = super(product_product, self).create(vals)
        if res:
            if not vals.get('barcode') and self.env['sale.config.settings'].search([], limit=1, order="id desc").gen_ean13:
                barcode_str = self.env['barcode.nomenclature'].sanitize_ean("%s%s" % (res.id, datetime.now().strftime("%d%m%y%H%M")))
                res.write({'barcode' : barcode_str})
        return res

    @api.model
    def calculate_product(self):
        self._cr.execute("""
                        SELECT count(id) from product_product where product_tmpl_id in (SELECT id FROM PRODUCT_TEMPLATE where available_in_pos='t' and sale_ok='t' and active='t') and active='t';
                        """)
        total_product = self._cr.fetchall()
        return total_product

    @api.model
    def get_product_price(self, pricelist_id, lines):
        pricelist = self.env['product.pricelist'].browse(pricelist_id)
        res = []
        if pricelist:
            for line in lines:
                new_price = pricelist.price_get(line.get('id'), line.get('qty'))
                if new_price:
                    res.append({
                        'product_id': line.get('id'),
                        'new_price': new_price.get(pricelist_id)
                    })
        if res:
            return res
        return False

class generate_product_ean13(models.TransientModel):
    _name = 'generate.product.ean13'

    overwrite_ean13 = fields.Boolean(String="Overwrite Exists Ean13")

    @api.multi
    def generate_product_ean13(self):
        for rec in self.env['product.product'].browse(self._context.get('active_ids')):
            if not self.overwrite_ean13 and rec.barcode:
                continue
            rec.write({'barcode': self.env['barcode.nomenclature'].sanitize_ean("%s%s" % (rec.id, datetime.now().strftime("%d%m%y%H%M")))
            })
        return True

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: