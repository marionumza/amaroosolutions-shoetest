# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class account_journal(models.Model):
    _inherit = "account.journal"

    pos_front_display = fields.Boolean('Display in POS Front', default=False)
    shortcut_key = fields.Char('Shortcut Key', size=1)
    debt = fields.Boolean(string='Debt Payment Method')
    apply_charges = fields.Boolean("Apply Charges");
    fees_amount = fields.Float("Fees Amount");
    fees_type = fields.Selection(selection=[('fixed','Fixed'),('percentage','Percentage')],string="Fees type", default="fixed")
    optional = fields.Boolean("Optional")
    is_cashdrawer = fields.Boolean("IS Cashdrawer")

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('voucher'):
            if self._context.get('journal_ids') and self._context.get('journal_ids')[0][2]:
                args = [[u'id', u'in', self._context.get('journal_ids')[0][2]]]
                return super(account_journal, self).name_search(name, args=args, operator=operator, limit=limit)
            return False
        else:
            return super(account_journal, self).name_search(name, args=args, operator=operator, limit=limit)

class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"
    
    @api.model
    def create(self,vals):
        invoice_obj = self.env['account.invoice']
        if vals.get('ref') and not vals.get('ref').startswith('POS'):
            invoice_id = invoice_obj.search([('origin','=',vals.get('ref'))],limit=1)
            if invoice_id and ((invoice_id.amount_total > 0 and vals.get('amount') > 0) or (invoice_id.amount_total < 0 and vals.get('amount') < 0)):
                vals.update({'name':invoice_id.number})
        res = super(AccountBankStatementLine, self).create(vals)
        return res
    
    
    @api.multi
    def write(self,vals):
        invoice_obj = self.env['account.invoice']
        if vals.get('ref') and not vals.get('ref').startswith('POS'):
            invoice_id = invoice_obj.search([('origin','=',vals.get('ref'))],limit=1)
            if invoice_id and ((invoice_id.amount_total > 0 and self.amount > 0) or (invoice_id.amount_total < 0 and self.amount < 0)):
                vals.update({'name':invoice_id.number})
        res = super(AccountBankStatementLine, self).write(vals)
        return res
        
    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        if not self._context.get('from_pos'):
            super(AccountBankStatementLine, self)._check_amount()

    @api.one
    @api.constrains('amount', 'amount_currency')
    def _check_amount_currency(self):
        if not self._context.get('from_pos'):
            super(AccountBankStatementLine, self)._check_amount_currency()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
