# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd.
#     (<http://acespritech.com>).
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
import uuid
import urlparse
import werkzeug.urls

class res_partner(models.Model):
    _inherit = "res.partner"
    
    @api.model
    def create(self,vals):
        if vals.get('email') and vals.get('is_share_info') == True:
            base_url = self.env['ir.config_parameter'].get_param('web.base.url')
            access_token = self.default_get(['access_token'])['access_token']
            vals.update({'access_token':access_token})
            url_one = urlparse.urljoin(
                base_url, 'mail/%(access_token)s/%(unsubscribe)s/unsubscribe?%(params)s' % {
                    'access_token': access_token,
                    'unsubscribe':'one',
                    'params': werkzeug.url_encode({'db': self.env.cr.dbname})
                }
            )
            
            url_two = urlparse.urljoin(
                base_url, 'mail/%(access_token)s/%(unsubscribe)s/unsubscribe?%(params)s' % {
                    'access_token': access_token,
                    'unsubscribe':'two',
                    'params': werkzeug.url_encode({'db': self.env.cr.dbname})
                }
            )
            subject = _('Marketing Contest')
            body = _(
                "HI %s, \n\n"
                "Thank you for registering to receive our newsletter.\n\n"
                "As a subscriber, we now look forward to sending you details of promotions, discounts and subscriber exclusive specials!.We’d also like to welcome you with an offer of 10 %% off your next full price purchase.\n\n"
                "Simply use the following discount code: ___________\n\n"
                "We do hope you have fun shopping, and look forward to seeing you again soon.\n\n"
                "Kind regards, \n\n"
                "Shoe Embassy Team \n\n \n\n"
                "<a href=%s>Unsubscribe</a>, from all communications. \n\n"
                "<a href=%s>Unsubscribe</a>, from marketing emails only. \n\n"
                "We don’t work with third parties. If you’ve received this without having given permission, please unsubscribe or contact us immediately on ask@shoeembassy.com."
                
            ) % (str(vals.get('name')),str(url_one),str(url_two))

            self.env['mail.mail'].create({
                'email_to': vals.get('email'),
                'subject': subject,
                'body_html': '<pre>%s</pre>' % body
            }).send()
        res = super(res_partner, self).create(vals)
        return res

    @api.multi
    def write(self,vals):
        if self.is_share_info == False and vals.get('is_share_info') == True:
            base_url = self.env['ir.config_parameter'].get_param('web.base.url')
            access_token = self.access_token
            vals.update({'access_token':access_token})
            url_one = urlparse.urljoin(
                base_url, 'mail/%(access_token)s/%(unsubscribe)s/unsubscribe?%(params)s' % {
                    'access_token': access_token,
                    'unsubscribe':'one',
                    'params': werkzeug.url_encode({'db': self.env.cr.dbname})
                }
            )
            
            url_two = urlparse.urljoin(
                base_url, 'mail/%(access_token)s/%(unsubscribe)s/unsubscribe?%(params)s' % {
                    'access_token': access_token,
                    'unsubscribe':'two',
                    'params': werkzeug.url_encode({'db': self.env.cr.dbname})
                }
            )
            subject = _('Marketing Contest')
            body = _(
                "HI %s, \n\n"
                "Thank you for registering to receive our newsletter.\n\n"
                "As a subscriber, we now look forward to sending you details of promotions, discounts and subscriber exclusive specials!.We’d also like to welcome you with an offer of 10 %% off your next full price purchase.\n\n"
                "Simply use the following discount code: ___________\n\n"
                "We do hope you have fun shopping, and look forward to seeing you again soon.\n\n"
                "Kind regards, \n\n"
                "Shoe Embassy Team \n\n \n\n"
                "<a href=%s>Unsubscribe</a>, from all communications. \n\n"
                "<a href=%s>Unsubscribe</a>, from marketing emails only. \n\n"
                "We don’t work with third parties. If you’ve received this without having given permission, please unsubscribe or contact us immediately on ask@shoeembassy.com."
                
            ) % (vals.get('name'),url_one,url_two)

            self.env['mail.mail'].create({
                'email_to': self.email,
                'subject': subject,
                'body_html': '<pre>%s</pre>' % body
            }).send()
        res = super(res_partner, self).write(vals)
        return res

    @api.multi
    def _get_debt(self):
        debt_account = self.env['account.account'].search([
            ('company_id', '=', self.env.user.company_id.id), ('code', '=', 'XDEBT')])
        debt_journal = self.env['account.journal'].search([
            ('company_id', '=', self.env.user.company_id.id), ('debt', '=', True)])
        if debt_account:
            self._cr.execute(
                """SELECT l.partner_id, SUM(l.debit - l.credit)
                FROM account_move_line l
                WHERE l.account_id = %s AND l.partner_id IN %s
                GROUP BY l.partner_id
                """,
                (debt_account.id, tuple(self.ids)))
    
            res = {}
            for partner in self:
                res[partner.id] = 0
            for partner_id, val in self._cr.fetchall():
                res[partner_id] += val

            if debt_journal:
                statements = self.env['account.bank.statement'].search(
                    [('journal_id', '=', debt_journal.id), ('state', '=', 'open')])
                if statements:
        
                    self._cr.execute(
                        """SELECT l.partner_id, SUM(l.amount)
                        FROM account_bank_statement_line l
                        WHERE l.statement_id IN %s AND l.partner_id IN %s
                        GROUP BY l.partner_id
                        """,
                        (tuple(statements.ids), tuple(self.ids)))
                    for partner_id, val in self._cr.fetchall():
                        res[partner_id] += val
                for partner in self:
                    partner.debt = res[partner.id]

    @api.one
    @api.depends('wallet_lines')
    def _calc_remaining(self):
        total = 0.00
        for s in self:
            for line in s.wallet_lines:
                total += line.credit - line.debit
        self.remaining_wallet_amount = total;

    @api.multi
    def _compute_remain_credit_limit(self):
        for partner in self:
            total_credited = 0
            orders = self.env['pos.order'].search([('partner_id', '=', partner.id),
                                                   ('state', '=', 'draft')])
            for order in orders:
                total_credited += order.amount_due
            partner.remaining_credit_limit = partner.credit_limit - total_credited
    
    is_share_info = fields.Boolean('Marketing Consent')
    gender = fields.Selection(selection=[('Male', 'Male'), ('Female', 'Female')], string='Gender M/F')
    access_token = fields.Char(
        'Security Token', copy=False, default=lambda self: str(uuid.uuid4()),
        required=True)
    debt = fields.Float(
        compute='_get_debt', string='Debt', readonly=True,
        digits=dp.get_precision('Account'), help='This debt value for only current company')
    wallet_lines = fields.One2many('wallet.management', 'customer_id', string="Wallet", readonly=True)
    remaining_wallet_amount = fields.Float(compute="_calc_remaining",string="Remaining Amount", readonly=True, store=True)
    prefer_ereceipt = fields.Boolean('Prefer E-Receipt')
    remaining_credit_limit = fields.Float("Remaining Credit Limit", compute="_compute_remain_credit_limit", default=0)

    @api.model
    def create_from_ui(self, partner):
        if partner.get('property_product_pricelist'):
            price_list_id = int(partner.get('property_product_pricelist'))
            partner.update({'property_product_pricelist': price_list_id})
        return super(res_partner, self).create_from_ui(partner)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: