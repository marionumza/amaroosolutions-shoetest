# -*- coding: utf-8 -*-


from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from calendar import monthrange


class BudgetManager(models.Model):
    _name = 'budget.manager'


    name = fields.Char(string='Name of the Budget')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.uid, string='User')
    date = fields.Date(default=lambda self: fields.Date.today(), string='Date')
    analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account')

    # sales
    sales_header_id = fields.Many2one('account.account.header',
        default=lambda self: self.env['ir.model.data'].xmlid_to_res_id('shoe_embassy_report.shoe_account_header_Sales'))
    sales_budget_line_ids = fields.One2many('budget.manager.line', 'sales_manager_id',
                                            string='Sales Budget Lines', copy=True)

    # costs
    costs_header_id = fields.Many2one('account.account.header',
        default=lambda self: self.env['ir.model.data'].xmlid_to_res_id('shoe_embassy_report.shoe_account_header_Controllable_Direct_Costs'))
    costs_budget_line_ids = fields.One2many('budget.manager.line', 'costs_manager_id',
                                            string='Sales Budget Lines', copy=True)

    # expences
    expences_header_id = fields.Many2one('account.account.header',
        default=lambda self: self.env['ir.model.data'].xmlid_to_res_id('shoe_embassy_report.shoe_account_header_Overheads'))
    expences_budget_line_ids = fields.One2many('budget.manager.line', 'expences_manager_id',
                                               string='Sales Budget Lines', copy=True)
    
    cost_of_goods_sum = fields.Html(compute='compute_cost_of_goods', String='Gross Profit')
    net_profit = fields.Html(compute='compute_net_profit', String='Net Profit')
    
    @api.multi
    @api.depends('sales_budget_line_ids', 'costs_budget_line_ids','expences_budget_line_ids')
    def compute_net_profit(self):
        for method in self:
            sales_budget_fields = {'d_0'+str(n):0 for n in range(1,10)}
            sales_budget_fields.update({'d_'+str(n):0 for n in range(10,32)})
            for sbl in method.sales_budget_line_ids:
                for i in range(1,10):
                    sales_budget_fields['d_0'+str(i)] = sales_budget_fields['d_0'+str(i)] + eval('sbl.d_0'+str(i))
                for i in range(10,32):
                    sales_budget_fields['d_'+str(i)] = sales_budget_fields['d_'+str(i)] + eval('sbl.d_'+str(i))
            
            for cbl in method.costs_budget_line_ids:
                for i in range(1,10):
                    sales_budget_fields['d_0'+str(i)] = sales_budget_fields['d_0'+str(i)] + eval('cbl.d_0'+str(i))
                for i in range(10,32):
                    sales_budget_fields['d_'+str(i)] = sales_budget_fields['d_'+str(i)] + eval('cbl.d_'+str(i))
                    
            for ebl in method.expences_budget_line_ids:
                for i in range(1,10):
                    sales_budget_fields['d_0'+str(i)] = sales_budget_fields['d_0'+str(i)] + eval('ebl.d_0'+str(i))
                for i in range(10,32):
                    sales_budget_fields['d_'+str(i)] = sales_budget_fields['d_'+str(i)] + eval('ebl.d_'+str(i))
                    
                    
            html_table = "" 
            for j in range(1,10):
                html_table=html_table+"""<td data-field="d_%s" class="o_list_number" style="">%s</td>   &nbsp;&nbsp;"""%(j,str(round(sales_budget_fields["d_0"+str(j)],2)))
                       
            for j in range(10,32):
                html_table=html_table+"""<td data-field="d_%s" class="o_list_number" style="">%s</td>   &nbsp;&nbsp;"""%(j,str(round(sales_budget_fields["d_"+str(j)], 2)))
                        
        self.net_profit = """<div class="table-responsive">
                       <table class="o_list_view table table-condensed table-striped">
                          <thead>
                             <tr>
                            
                                <th data-id="d_01" class="o_column_sortable">
                                   01
                                </th>
                                <th data-id="d_02" class="o_column_sortable">
                                   02
                                </th>
                                <th data-id="d_03" class="o_column_sortable">
                                   03
                                </th>
                                <th data-id="d_04" class="o_column_sortable">
                                   04
                                </th>
                                <th data-id="d_05" class="o_column_sortable">
                                   05
                                </th>
                                <th data-id="d_06" class="o_column_sortable">
                                   06
                                </th>
                                <th data-id="d_07" class="o_column_sortable">
                                   07
                                </th>
                                <th data-id="d_08" class="o_column_sortable">
                                   08
                                </th>
                                <th data-id="d_09" class="o_column_sortable">
                                   09
                                </th>
                                <th data-id="d_10" class="o_column_sortable">
                                   10
                                </th>
                                <th data-id="d_11" class="o_column_sortable">
                                   11
                                </th>
                                <th data-id="d_12" class="o_column_sortable">
                                   12
                                </th>
                                <th data-id="d_13" class="o_column_sortable">
                                   13
                                </th>
                                <th data-id="d_14" class="o_column_sortable">
                                   14
                                </th>
                                <th data-id="d_15" class="o_column_sortable">
                                   15
                                </th>
                                <th data-id="d_16" class="o_column_sortable">
                                   16
                                </th>
                                <th data-id="d_17" class="o_column_sortable">
                                   17
                                </th>
                                <th data-id="d_18" class="o_column_sortable">
                                   18
                                </th>
                                <th data-id="d_19" class="o_column_sortable">
                                   19
                                </th>
                                <th data-id="d_20" class="o_column_sortable">
                                   20
                                </th>
                                <th data-id="d_21" class="o_column_sortable">
                                   21
                                </th>
                                <th data-id="d_22" class="o_column_sortable">
                                   22
                                </th>
                                <th data-id="d_23" class="o_column_sortable">
                                   23
                                </th>
                                <th data-id="d_24" class="o_column_sortable">
                                   24
                                </th>
                                <th data-id="d_25" class="o_column_sortable">
                                   25
                                </th>
                                <th data-id="d_26" class="o_column_sortable">
                                   26
                                </th>
                                <th data-id="d_27" class="o_column_sortable">
                                   27
                                </th>
                                <th data-id="d_28" class="o_column_sortable">
                                   28
                                </th>
                                <th data-id="d_29" class="o_column_sortable">
                                   29
                                </th>
                                <th data-id="d_30" class="o_column_sortable">
                                   30
                                </th>
                                <th data-id="d_31" class="o_column_sortable">
                                   31
                                </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr data-id="3" class="" style="">"""+html_table+"""
                             </tr>
                          </tbody>
                       </table>
                    </div>"""
    
    @api.multi
    @api.depends('sales_budget_line_ids', 'costs_budget_line_ids')
    def compute_cost_of_goods(self):
        for method in self:
            sales_budget_fields = {'d_0'+str(n):0 for n in range(1,10)}
            sales_budget_fields.update({'d_'+str(n):0 for n in range(10,32)})
            for sbl in method.sales_budget_line_ids:
                for i in range(1,10):
                    sales_budget_fields['d_0'+str(i)] = sales_budget_fields['d_0'+str(i)] + eval('sbl.d_0'+str(i))
                for i in range(10,32):
                    sales_budget_fields['d_'+str(i)] = sales_budget_fields['d_'+str(i)] + eval('sbl.d_'+str(i))
            
            for cbl in method.costs_budget_line_ids:
                for i in range(1,10):
                    sales_budget_fields['d_0'+str(i)] = sales_budget_fields['d_0'+str(i)] + eval('cbl.d_0'+str(i))
                for i in range(10,32):
                    sales_budget_fields['d_'+str(i)] = sales_budget_fields['d_'+str(i)] + eval('cbl.d_'+str(i))
            html_table = "" 
            for j in range(1,10):
                html_table=html_table+"""<td data-field="d_%s" class="o_list_number" style="">%s</td>   &nbsp;&nbsp;"""%(j,str(round(sales_budget_fields["d_0"+str(j)],2)))
                       
            for j in range(10,32):
                html_table=html_table+"""<td data-field="d_%s" class="o_list_number" style="">%s</td>   &nbsp;&nbsp;"""%(j,str(round(sales_budget_fields["d_"+str(j)], 2)))
                        
        self.cost_of_goods_sum = """<div class="table-responsive">
                       <table class="o_list_view table table-condensed table-striped">
                          <thead>
                             <tr>
                                <th data-id="d_01" class="o_column_sortable">
                                   01
                                </th>
                                <th data-id="d_02" class="o_column_sortable">
                                   02
                                </th>
                                <th data-id="d_03" class="o_column_sortable">
                                   03
                                </th>
                                <th data-id="d_04" class="o_column_sortable">
                                   04
                                </th>
                                <th data-id="d_05" class="o_column_sortable">
                                   05
                                </th>
                                <th data-id="d_06" class="o_column_sortable">
                                   06
                                </th>
                                <th data-id="d_07" class="o_column_sortable">
                                   07
                                </th>
                                <th data-id="d_08" class="o_column_sortable">
                                   08
                                </th>
                                <th data-id="d_09" class="o_column_sortable">
                                   09
                                </th>
                                <th data-id="d_10" class="o_column_sortable">
                                   10
                                </th>
                                <th data-id="d_11" class="o_column_sortable">
                                   11
                                </th>
                                <th data-id="d_12" class="o_column_sortable">
                                   12
                                </th>
                                <th data-id="d_13" class="o_column_sortable">
                                   13
                                </th>
                                <th data-id="d_14" class="o_column_sortable">
                                   14
                                </th>
                                <th data-id="d_15" class="o_column_sortable">
                                   15
                                </th>
                                <th data-id="d_16" class="o_column_sortable">
                                   16
                                </th>
                                <th data-id="d_17" class="o_column_sortable">
                                   17
                                </th>
                                <th data-id="d_18" class="o_column_sortable">
                                   18
                                </th>
                                <th data-id="d_19" class="o_column_sortable">
                                   19
                                </th>
                                <th data-id="d_20" class="o_column_sortable">
                                   20
                                </th>
                                <th data-id="d_21" class="o_column_sortable">
                                   21
                                </th>
                                <th data-id="d_22" class="o_column_sortable">
                                   22
                                </th>
                                <th data-id="d_23" class="o_column_sortable">
                                   23
                                </th>
                                <th data-id="d_24" class="o_column_sortable">
                                   24
                                </th>
                                <th data-id="d_25" class="o_column_sortable">
                                   25
                                </th>
                                <th data-id="d_26" class="o_column_sortable">
                                   26
                                </th>
                                <th data-id="d_27" class="o_column_sortable">
                                   27
                                </th>
                                <th data-id="d_28" class="o_column_sortable">
                                   28
                                </th>
                                <th data-id="d_29" class="o_column_sortable">
                                   29
                                </th>
                                <th data-id="d_30" class="o_column_sortable">
                                   30
                                </th>
                                <th data-id="d_31" class="o_column_sortable">
                                   31
                                </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr data-id="3" class="" style="">"""+html_table+"""
                             </tr>
                          </tbody>
                       </table>
                    </div>"""

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        for val in self._context.keys():
            if val in ['analytic_id'] and self._context[val]:
                    args.append((val, '=', self._context[val].id))
        return super(BudgetManager, self).search(args, offset=offset, limit=limit, order=order, count=count)
            
    def write(self, vals):
        # for line in self.budget_line_ids:
        #     line.apply_changes()
        return super(BudgetManager, self).write(vals)

class BudgetManagerLine(models.Model):
    _name = 'budget.manager.line'
    _rec_name = 'header_id'

    def apply_changes(self):
        if self.sales_manager_id:
            manager_id = 'sales_manager_id'
            sign = 1
        elif self.costs_manager_id:
            manager_id = 'costs_manager_id'
            sign = -1
        else:
            manager_id = 'expences_manager_id'
            sign = -1

        date = datetime.strptime(self[manager_id].date, "%Y-%m-%d")
        if self.method == 'M2':
            values = {}
            for day in range(0, monthrange(date.year, date.month)[1], 1):
                values['d_' + format(day+1, '02')] = (self.value/100) * self.line_id['d_' + format(day+1, '02')]

            # values = {
            #     'd_01': (self.value/100) * self.line_id.d_01,
            #     'd_02': (self.value/100) * self.line_id.d_02,
            #     'd_03': (self.value/100) * self.line_id.d_03,
            #     'd_04': (self.value/100) * self.line_id.d_04,
            #     'd_05': (self.value/100) * self.line_id.d_05,
            #     'd_06': (self.value/100) * self.line_id.d_06,
            #     'd_07': (self.value/100) * self.line_id.d_07,
            #     'd_08': (self.value/100) * self.line_id.d_08,
            #     'd_09': (self.value/100) * self.line_id.d_09,
            #     'd_10': (self.value/100) * self.line_id.d_10,
            #     'd_11': (self.value/100) * self.line_id.d_11,
            #     'd_12': (self.value/100) * self.line_id.d_12,
            #     'd_13': (self.value/100) * self.line_id.d_13,
            #     'd_14': (self.value/100) * self.line_id.d_14,
            #     'd_15': (self.value/100) * self.line_id.d_15,
            #     'd_16': (self.value/100) * self.line_id.d_16,
            #     'd_17': (self.value/100) * self.line_id.d_17,
            #     'd_18': (self.value/100) * self.line_id.d_18,
            #     'd_19': (self.value/100) * self.line_id.d_19,
            #     'd_20': (self.value/100) * self.line_id.d_20,
            #     'd_21': (self.value/100) * self.line_id.d_21,
            #     'd_22': (self.value/100) * self.line_id.d_22,
            #     'd_23': (self.value/100) * self.line_id.d_23,
            #     'd_24': (self.value/100) * self.line_id.d_24,
            #     'd_25': (self.value/100) * self.line_id.d_25,
            #     'd_26': (self.value/100) * self.line_id.d_26,
            #     'd_27': (self.value/100) * self.line_id.d_27,
            #     'd_28': (self.value/100) * self.line_id.d_28,
            #     'd_29': (self.value/100) * self.line_id.d_29,
            #     'd_30': (self.value/100) * self.line_id.d_30,
            #     'd_31': (self.value/100) * self.line_id.d_31,
            # }
            self.write(values)
        elif self.method == 'M3':

            def map_month(date, year, month):
                mapped_list = {}
                for day in range(0, monthrange(year, month)[1], 1):
                    # monday 0
                    mapped_list[format(day+1, '02')] = (date + timedelta(days=day)).weekday()
                return mapped_list

            mapped_list = map_month(date, date.year, date.month)
            week_template_sum = 0
            for key, entry in mapped_list.iteritems():
                week_template_sum += self.template_id['day_' + str(format(entry, '02'))]
            if week_template_sum == 0:
                raise ValidationError(_('Invalid Wedek Template'))
            unit_value = self.value / week_template_sum
            line_values = {}
            for key, entry in mapped_list.iteritems():
                a = 'd_' + key
                b = self.template_id['day_' + str(format(entry, '02'))]
                line_values[a] = b * unit_value * sign
            self.write(line_values)
        return True

    def open_sales_line(self):
        view = self.env.ref('budget_manager.view_budget_manager_sales_line_form')
        return {
            'name': _('Budget manager line'),
            'res_model': 'budget.manager.line',
            # 'context': {
            #     'default_last_activity_id': self.next_activity_id.id,
            #     'default_lead_id': self.lead_id.id
            # },
            'type': 'ir.actions.act_window',
            'view_id': False,
            'views': [(view.id, 'form')],
            'view_mode': 'form',
            'target': 'new',
            'view_type': 'form',
            'res_id': self.id
        }

    def open_costs_line(self):
        view = self.env.ref('budget_manager.view_budget_manager_costs_line_form')
        return {
            'name': _('Budget manager line'),
            'res_model': 'budget.manager.line',
            # 'context': {
            #     'default_last_activity_id': self.next_activity_id.id,
            #     'default_lead_id': self.lead_id.id
            # },
            'type': 'ir.actions.act_window',
            'view_id': False,
            'views': [(view.id, 'form')],
            'view_mode': 'form',
            'target': 'new',
            'view_type': 'form',
            'res_id': self.id
        }

    def open_expences_line(self):
        view = self.env.ref('budget_manager.view_budget_manager_expences_line_form')
        return {
            'name': _('Budget manager line'),
            'res_model': 'budget.manager.line',
            # 'context': {
            #     'default_last_activity_id': self.next_activity_id.id,
            #     'default_lead_id': self.lead_id.id
            # },
            'type': 'ir.actions.act_window',
            'view_id': False,
            'views': [(view.id, 'form')],
            'view_mode': 'form',
            'target': 'new',
            'view_type': 'form',
            'res_id': self.id
        }

    def _get_total(self):
        for line in self:
            line.total = line.d_01 + line.d_02 + line.d_03 + line.d_04 + line.d_05 + \
                         line.d_06 + line.d_07 + line.d_08 + line.d_09 + line.d_10 + \
                         line.d_11 + line.d_12 + line.d_13 + line.d_14 + line.d_15 + \
                         line.d_16 + line.d_17 + line.d_18 + line.d_19 + line.d_20 + \
                         line.d_21 + line.d_22 + line.d_23 + line.d_24 + line.d_25 + \
                         line.d_26 + line.d_27 + line.d_28 + line.d_29 + line.d_30 + \
                         line.d_31

    def update_manager(self):
        for line in self.search([]):
            line.manager_id = line.sales_manager_id.id or line.costs_manager_id.id or line.expences_manager_id.id


    @api.depends('sales_manager_id', 'costs_manager_id', 'expences_manager_id')
    def get_manager(self):
        for line in self:
            line.manager_id = line.sales_manager_id.id or line.costs_manager_id.id or line.expences_manager_id.id

    manager_id = fields.Many2one('budget.manager', compute=get_manager, string='Budget Manager', store=True)
    sales_manager_id = fields.Many2one('budget.manager', string='Budget Manager(S)', ondelete='cascade')
    costs_manager_id = fields.Many2one('budget.manager', string='Budget Manager(C)', ondelete='cascade')
    expences_manager_id = fields.Many2one('budget.manager', string='Budget Manager(E)', ondelete='cascade')
    header_id = fields.Many2one('account.account.header', string='Header',
                                domain=[('parent_id', '!=', False), ('child_ids', '=', False)])
    method = fields.Selection([
        ('M1', 'Direct Input'),
        ('M2', '% of Line'),
        ('M3', 'Week Template')],default='M1', string='Method')
    line_id = fields.Many2one('budget.manager.line', string='Line')
    template_id = fields.Many2one('budget.week.template', string='Week Template')
    value = fields.Float('Value')
    d_01 = fields.Float(string='01')
    d_02 = fields.Float(string='02')
    d_03 = fields.Float(string='03')
    d_04 = fields.Float(string='04')
    d_05 = fields.Float(string='05')
    d_06 = fields.Float(string='06')
    d_07 = fields.Float(string='07')
    d_08 = fields.Float(string='08')
    d_09 = fields.Float(string='09')
    d_10 = fields.Float(string='10')
    d_11 = fields.Float(string='11')
    d_12 = fields.Float(string='12')
    d_13 = fields.Float(string='13')
    d_14 = fields.Float(string='14')
    d_15 = fields.Float(string='15')
    d_16 = fields.Float(string='16')
    d_17 = fields.Float(string='17')
    d_18 = fields.Float(string='18')
    d_19 = fields.Float(string='19')
    d_20 = fields.Float(string='20')
    d_21 = fields.Float(string='21')
    d_22 = fields.Float(string='22')
    d_23 = fields.Float(string='23')
    d_24 = fields.Float(string='24')
    d_25 = fields.Float(string='25')
    d_26 = fields.Float(string='26')
    d_27 = fields.Float(string='27')
    d_28 = fields.Float(string='28')
    d_29 = fields.Float(string='29')
    d_30 = fields.Float(string='30')
    d_31 = fields.Float(string='31')
    total = fields.Float(compute=_get_total, string='Total')

    @api.one
    @api.constrains('value')
    def _check_values(self):
        if self.method != 'M1' and self.value == 0.0:
            raise Warning(_('Value should not be zero.'))



class BudgetWeekTemplate(models.Model):
    _name = 'budget.week.template'

    name = fields.Char(string='Name')
    day_00 = fields.Float('Monday')
    day_01 = fields.Float('Tuesday')
    day_02 = fields.Float('Wednesday')
    day_03 = fields.Float('Thursday')
    day_04 = fields.Float('Friday')
    day_05 = fields.Float('Saturday')
    day_06 = fields.Float('Sunday')
