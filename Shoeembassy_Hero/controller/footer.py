from odoo import http
from odoo.http import request
from odoo import SUPERUSER_ID
from odoo import models, fields, api


class footerdata(http.Controller):

    @http.route(['/showfooter_data'],type='json', auth='public', website=True)
    def footer(self, template):
        data = request.env['footer.footer'].sudo().search([('is_active', '=', 'True')])

        return request.env.ref(template).render({'footer':data})

    @http.route(['/strore_data'],type='json', auth='public', website=True)
    def store(self, template):
        store_data = request.env['stock.warehouse'].sudo().search([('is_active', '=', 'True')])
        return request.env.ref(template).render({'store':store_data})

    @http.route(['/socialmedia_data'],type='json', auth='public', website=True)
    def social_media(self, template):
        facebook_data = request.env['website'].sudo().search([])
        return request.env.ref(template).render({'facebook':facebook_data})