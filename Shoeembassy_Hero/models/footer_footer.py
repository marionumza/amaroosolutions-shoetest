# -*- coding: utf-8 -*-

from odoo import models, fields


class Footer(models.Model):

	_name = 'footer.footer'
	_rec_name = 'name'
	_order = 'sequence'

	sequence = fields.Integer(string='Sequence')
	name = fields.Char(string='Name')
	is_active = fields.Boolean('Active')
	url = fields.Char('URL')
	
# class AlternativeProducts(models.Model):
#     _name = "alternative.product.update"
#     
#     name = fields.Char(string="Name", required=True)
#     
#     @api.multi
#     def update_products(self):
#         product_template_obj = self.env['product.template']
#         product_tmpl_ids = product_template_obj.search_read([], ['alternative_product_ids','model_id'])
#         for product_template in product_tmpl_ids:
#             if product_template['model_id']:
#                 same_product_ids = product_template_obj.search([('model_id.name','=',product_template['model_id'][1])])
#                 if product_template['id'] in same_product_ids.ids:
#                     same_product_ids.ids.remove(product_template['id'])
#                 product_obj = product_template_obj.browse(product_template['id'])
#                 update_data = product_obj.write({'alternative_product_ids':[(6,0,same_product_ids.ids+product_template['alternative_product_ids'])]})
#         return True