# -*- coding: utf-8 -*-

from odoo import api, fields, models


class Website_custom(models.Model):

    _inherit = "website"

    social_instagram = fields.Char('Instagram Account')
    social_pinterest = fields.Char('Pintrest Account')
    social_trust = fields.Char('Trustpilot Account')

    def category_check(self, filter=[]):
        return self.env['product.public.category'].sudo().search([])

    def get_offers(self):
		return self.env['our.offers'].sudo().search([])

    def get_search_category(self):
        return self.env['product.public.category'].sudo().search([('is_search_categ', '=', True)])
    
    # def stores(self):
    # 	return self.env['stock.warehouse'].sudo().search([])

    # def footer(self):
    #     return self.env['footer.footer'].sudo().search([])

class website_config_settings(models.TransientModel):

    _inherit = "website.config.settings"

    social_instagram = fields.Char(related='website_id.social_instagram')
    social_pinterest = fields.Char(related='website_id.social_pinterest')
    social_trustpilot = fields.Char(related='website_id.social_trust')

class website_menu(models.Model):

    _inherit = 'website.menu'

    is_active = fields.Boolean('Active', default='True')