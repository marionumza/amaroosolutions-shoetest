# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _
from odoo.exceptions import  ValidationError

class ProductPublicCategory(models.Model):

    _inherit = 'product.public.category'
    _order = 'sequence'

    is_active = fields.Boolean('Active')
    is_asc_subcateg = fields.Boolean('Asc Subcategory')
    show_category_image = fields.Boolean('Show Category Image')
    background_color = fields.Char('Background Color', help="Here you can set a "
    "specific HTML color index (e.g. #ff0000) to set the background color of sub category.")
    is_search_categ = fields.Boolean('Search Category')
    attribute_value_ids = fields.Many2many(
        'product.attribute.value', string='Attributes', ondelete='restrict')
    banner_image = fields.Binary("Category Banner Image")
    mobile_banner_image = fields.Binary("Mobile Category Banner Image")
    gender_id = fields.Many2one('gender', string='Type',required=True)
    
    @api.multi
    @api.constrains('is_search_categ')
    def _check_search_categ(self):
        if len(self.search([]).filtered(lambda c: c.is_search_categ))>1:
            raise ValidationError(_('There can be only 1 search category'))

    @api.multi
    def get_size_correlation(self,type):
#         gender_name = self.name.lower()
#         men_gender_id = self.env['gender'].sudo().search([('name','=','Mens')])
#         women_gender_id = self.env['gender'].sudo().search([('name','=','Womens')])
        vals = {}
#         if self.attribute_value_ids:
#             gender_id = False
#             if women_gender_id.name.startswith('Womens'):
#                 gender_id = women_gender_id
#             if women_gender_id.name.startswith('Mens'):
#                 gender_id = men_gender_id
        for val in self.attribute_value_ids:
            size_id = self.env['size.correlation'].sudo().search([('eu','=',val.name),('gender_id','=',self.gender_id.id)])
            if size_id and type == 'uk':
                vals.update({val.id:size_id.uk})
            elif size_id and type == 'us':
                vals.update({val.id:size_id.us})
            else:
                vals.update({val.id:size_id.eu})
        return vals
    