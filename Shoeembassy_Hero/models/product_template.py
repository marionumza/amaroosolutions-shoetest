# -*- coding: utf-8 -*-

from odoo import api, fields, models
import random


class  ProductProduct(models.Model):
    
    _inherit = 'product.product'

    @api.multi
    def get_product_uk_size(self):
        for product in self:
            size_id = self.env['size.correlation'].sudo().search([('gender_id','=',product.gender_id.id),('eu','=',product.attribute_value_ids and product.attribute_value_ids[0].name or False)])
            if size_id:
                return size_id.uk
        return False
    
class  ProductTemplate(models.Model):
    
    _inherit = 'product.template'
    
#     @api.multi
#     def get_product_stockble_data(self):
#         for attribute_line in self.attribute_line_ids.sorted(key=lambda x: x.attribute_id.sequence):
#             for value_id in attribute_line.value_ids:
#                 products = self.env['product.product'].sudo().search([('product_tmpl_id','=',self.id),('attribute_value_ids','in',value_id.ids),('qty_available','>',0)])
#                 if products:
#                     return value_id
#         return False
    
    @api.multi
    def get_ramdom_rec_products(self):
        product_ids = self.search([("website_published", "=", True),("public_categ_ids", "!=", False),('shoe_type_id','=',self.shoe_type_id and self.shoe_type_id.id or False)])
        if product_ids:
            random_value = random.randint(0,len(product_ids)-4)
            return product_ids[random_value:random_value+4]
        return False
    
    # product_brand_name = fields.Char('Brand Name')
    # product_color = fields.Char('Color')
    # recommend_product_ids = fields.Many2many('product.template', string='Recommend Product')
    recommend_product_ids = fields.Many2many('product.template', 'product_recommend_rel', 'recommend_src_id', 'recommend_dest_id',
                                               string='Recommend Products')

    @api.multi
    def check_product_quantity(self, variant_id, value_id):
    	quantity = 0
    	if variant_id and value_id:
    		product_id = self.env['product.product'].sudo().search([('product_tmpl_id','=',self.id),('attribute_value_ids','in',value_id.ids)])
    		quantity = product_id and product_id[0] and product_id[0].sudo().qty_available
    	return quantity

    @api.multi
    def get_product_images(self):
    	product_type = ['c','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15']
    	image_list = []
    	for p_type in product_type:
    		for image in self.sudo().images:
    			if p_type == str(image.type):
    				if len(image_list) < 6:
    					image_list.append(image.id)
    	return self.env['product.image'].browse(image_list)

    @api.multi
    def get_uk_us_size(self, value_id, size_type):
        vals = {}
        size_id = self.env['size.correlation'].sudo().search([('gender_id','=',self.gender_id.id),('eu','=',value_id.name)])
        if size_type =='uk':
            vals.update({'uk_size':size_id and size_id[0]})
        if size_type =='us':
            vals.update({'us_size':size_id and size_id[0]})
        return vals