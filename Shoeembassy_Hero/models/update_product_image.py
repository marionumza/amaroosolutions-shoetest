# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import urllib
import logging
import base64
import urlparse
_logger = logging.getLogger(__name__)

class UpdateProductImage(models.Model):
    _name = "update.product.image"
    
    name = fields.Char(string="Name", required=True)
    
    @api.multi
    def update_image(self):
        product_image_obj = self.env['product.image']
        product_img_ids = product_image_obj.search_read([], ['image_url','image'])
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        for product_img in product_img_ids:
            if not product_img['image'] and product_img['image_url']:
                try:
                    (filename, header) = urllib.urlretrieve(product_img['image_url'])
                    with open(filename, 'rb') as f:
                        image_record = product_image_obj.browse(product_img['id'])
                        image_url = urlparse.urljoin(
                            base_url, '/web/image?model=product.image&id=%(id)s&field=image' % {
                                'id': product_img['id'],
                            }
                        )
                        update_data = image_record.write({'image':base64.b64encode(f.read()),'image_url':image_url})
                except:
                    _logger.error("URL %s cannot be fetched", product_img['image_url'],
                                  exc_info=True)
                
        return True
