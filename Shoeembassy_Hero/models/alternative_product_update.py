# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class AlternativeProducts(models.Model):
    _name = "alternative.product.update"
    
    name = fields.Char(string="Name", required=True)
    
    @api.multi
    def update_products(self):
        product_template_obj = self.env['product.template']
        product_tmpl_ids = product_template_obj.search_read([], ['alternative_product_ids','model_id'])
        for product_template in product_tmpl_ids:
            if product_template['model_id']:
                same_product_ids = product_template_obj.search([('id','!=',product_template['id']),('model_id.name','=',product_template['model_id'][1])])
                product_obj = product_template_obj.browse(product_template['id'])
                update_data = product_obj.write({'alternative_product_ids':[(6,0,same_product_ids.ids+product_template['alternative_product_ids'])]})
        return True
