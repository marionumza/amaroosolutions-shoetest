# -*- coding: utf-8 -*-
from . import warehouse
from . import our_offer_policy
from . import product_public_category
from . import website
from . import footer_footer
from . import wishlist
from . import product_attribute
from . import product_template
from . import alternative_product_update
from . import update_product_image
from . import parallax