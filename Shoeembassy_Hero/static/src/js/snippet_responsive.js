jQuery(document).ready(function() {
	// IF the first element is banner or category images then no padding will be applied
	// That is, it will stay behind the main menu
	
	if ($('div').hasClass("category_mobile_banner_image visible-xs")) {
		 $('.menu_inner_area .navbar.navbar-default .nav.navbar-nav').css('text-align', 'center');
	 }
	if ($('section').hasClass("custom_product_page")) {
		 $('.menu_inner_area .navbar.navbar-default .nav.navbar-nav').css('text-align', 'center');
	 }
	$( document ).on( "myCustomEvent", {}, function( event ) {
	    var carousel = $('#wrap').find('.carousel')
		var carousel_index = $('#wrap').children().index( carousel )
		//console.log("$('.category_main_banner').length",$('.category_main_banner').length, carousel_index)
		var padding = $('.header_top').height() + $('.navbar-nav').height()

		if($(window).width() < 523){
			padding = padding + 10
		}else{
			if ($('.navbar-nav').height() <88){
				padding +=  $('.custom_center_nav').height()
				}else{
					padding +=  30
				}
		}
		$( window ).resize(function() {
			console.log("$('.category_main_banner').length",$('.category_main_banner').length, carousel_index)
			var padding = $('.header_top').height() + $('.navbar-nav').height()

			if($(window).width() < 523){
				padding = padding + 10
			}else{
				if ($('.navbar-nav').height() <88){
				padding +=  $('.custom_center_nav').height()
				}else{
					padding +=  30
				}
			}
//			if((carousel_index==-1 || carousel_index>1) || $('.category_main_banner').length<=0 ){
//				$('#wrap').css('padding-top', String(padding) + 'px')
//			}
			$('.search_formds').css('padding-top', String(padding + 10) + 'px')

		});
//		if((carousel_index==-1 || carousel_index>1) && $('.category_main_banner').length<1 ){
//			$('#wrap').css('padding-top', String(padding) + 'px')
//		}

        if ($(window).width() > 1100){
            padding = padding - 75;
            $('#search-text').attr("placeholder", "I'm looking for...");
        }
        else{
            $('#search-text').attr("placeholder", "Search...");
        }

		$('.search_formds').css('padding-top', String(padding + 10) + 'px')
	});
 
$( document ).trigger( "myCustomEvent" );
});
