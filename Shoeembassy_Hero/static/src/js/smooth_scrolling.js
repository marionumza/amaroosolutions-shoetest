odoo.define('Shoeembassy_Hero.smoot_scrolling', function(require) {
    "use strict";

    var base = require('web_editor.base');
    var ajax = require('web.ajax');
    var utils = require('web.utils');
    var core = require('web.core');
    var config = require('web.config');
    var Model = require('web.Model');
    var _t = core._t;

    jQuery(document).ready(function() {
        "#map" == window.location.hash && setTimeout(function() {
                $("div .map_icon a").click()
            }, 3e3), $(".map_icon a").click(function(o) {
                var t = ($(".our_store_wrapper").offset() || {
                    top: NaN
                }).top;
                isNaN(t) ? (window.location.href = "/#map", $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset()
                }, "slow"), window.location.href = "#map") : ($(window).width() > 768 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 125
                }, "slow"), $(window).width() > 500 && $(window).width() < 767 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 182
                }, "slow"), $(window).width() > 351 && $(window).width() < 499 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 180
                }, "slow"), $(window).width() > 318 && $(window).width() < 350 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 180
                }, "slow"))
            }), $("div .scrolldown a").click(function(o) {
                var t = ($(".our_store_wrapper").offset() || {
                    top: NaN
                }).top;
                isNaN(t) ? (window.location.href = "/#map", $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset()
                }, "slow"), window.location.href = "#map") : ($(window).width() > 768 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 125
                }, "slow"), $(window).width() > 500 && $(window).width() < 767 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 182
                }, "slow"), $(window).width() > 351 && $(window).width() < 499 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 180
                }, "slow"), $(window).width() > 318 && $(window).width() < 350 && $("html,body").animate({
                    scrollTop: $(".our_store_wrapper").offset().top - 180
                }, "slow"))
            }),
            $(window).resize(function() {
                var o = $(".media-img-3").height() + $(".media-img-4").height() + 10;
                $(".gallery_center_image a img").height(o)
                var h_inn = $('.menu_inner_area').height();
                var width = $(window).width();
                if (width >= 768) {
                    var x_inn = h_inn + 40;
                    var y_inn = x_inn + 33;
                    $('main .oe_website_sale').attr('style', 'margin-top:' + x_inn + 'px !important;');
                    // $('main .custom-account').attr('style', 'margin-top:' + x_inn + 'px !important;');
                } else {
                    $('main .oe_website_sale').attr('style', 'margin-top:' + h_inn + 'px !important;');
                    // $('main .custom-account').attr('style', 'margin-top:' + h_inn + 'px !important;');
                }
               /* var top_menu_height = $('#top_menu').height();
                if (top_menu_height > 88){
                    $('.menu_inner_area .navbar.navbar-default .nav.navbar-nav li a').attr('style','line-height:40px;');
                }
                if (top_menu_height < 88){
                    $('.menu_inner_area .navbar.navbar-default .nav.navbar-nav li a').attr('style','line-height:86px;');
                }*/
            })
        var width = $(window).width();
        var h_inn = $('.menu_inner_area').height();
        if (width >= 768) {
            var x_inn = h_inn + 40;
            $('main .oe_website_sale').attr('style', 'margin-top:' + x_inn + 'px !important;');
            // $('main .custom-account').attr('style', 'margin-top:' + x_inn + 'px !important;');
        } else {
            $('main .oe_website_sale').attr('style', 'margin-top:' + h_inn + 'px !important;');
            // $('main .custom-account').attr('style', 'margin-top:' + h_inn + 'px !important;');
        }
        /*var top_menu_height = $('#top_menu').height();
        var header_top = $('.header_top').height();
        $('.category_main_banner').attr('style', 'margin-top:' + header_top + 'px !important;');
        if (top_menu_height > 88){
            $('.menu_inner_area .navbar.navbar-default .nav.navbar-nav li a').attr('style','line-height:40px;');
        }
        if (top_menu_height < 88){
            $('.menu_inner_area .navbar.navbar-default .nav.navbar-nav li a').attr('style','line-height:86px;');
        }*/

        function openSearch() {
            document.getElementById("myOverlay").style.display = "block";
        }

        function closeSearch() {
            document.getElementById("myOverlay").style.display = "none";
        }

        $('.variant_radio').click(function(){
            $('.size_inner li').each(function () {
                if ($(this).hasClass('active')){
                    $(this).removeClass('active');
                    $(this).find('input.js_variant_change').removeAttr('checked');
                }
            });
            $(this).parent('li').addClass('active')
            $(this).next().click();
        });
        //$('#product_detail').attr('style','margin-top:0px !important;')
        $('.stock_info_div').hide();
        $('#add_to_cart').show();
    });

    $('.search_icon').on('click', function(e){
        $('.search_formds').removeClass('hidden');
        $('.oe_search_box').focus()
        $('.search_formds').addClass('change-size'),function(){
            $('.search_formds').removeClass('change-size');
        };
        $(this).addClass('hidden');
        $('.search_icon_close').removeClass('hidden');
        $('#wrap').css('padding', '0px')
        $( document ).trigger( "myCustomEvent" );
    });
    $('.search_icon_close').on('click', function(e){
        $('.search_formds').addClass('hidden');

        $('.search_icon').removeClass('hidden');
        $(this).addClass('hidden');
        $( document ).trigger( "myCustomEvent" );

    });
    $('.oe_website_sale').each(function () {
        var oe_website_sale = this;
        $(oe_website_sale).on('change', 'input.js_variant_change', function (ev) {
            var attribute_value = $(ev.target).parent().find('a.variant_radio').text();
            var domain = [["gender_id", "=", parseInt($('.product_gender').val())],["eu",'=',attribute_value]];
            ajax.jsonRpc("/get_size_corelation_data", 'call', {'domain': domain,'fields':['us','uk','length','eu']}).then(function(result) {
                if (result){
                    $('#eu_size').val(attribute_value);
                    $('#uk_size').val(result.uk);
                    $('#us_size').val(result.us);
                    $('#centimeters').val(result.length);
                }
            });
        });

        $(oe_website_sale).on('change', 'input#cls_uk_size_ul_id', function (ev) {
        	var attribute_value = $(ev.target).parent().find('a.variant_radio').text();
            var domain = [["gender_id", "=", parseInt($('.product_gender').val())],["uk",'=',attribute_value]];
            ajax.jsonRpc("/get_size_corelation_data", 'call', {'domain': domain,'fields':['us','uk','length','eu']}).then(function(result) {
                if (result){
                    $('#eu_size').val(result.eu);
                    $('#uk_size').val(result.uk);
                    $('#us_size').val(result.us);
                    $('#centimeters').val(result.length);
                }
            });
        });

        $(oe_website_sale).on('change', 'input#cls_us_size_ul_id', function (ev) {
            var attribute_value = $(ev.target).parent().find('a.variant_radio').text();
            var domain = [["gender_id", "=", parseInt($('.product_gender').val())],["us",'=',attribute_value]];
            ajax.jsonRpc("/get_size_corelation_data", 'call', {'domain': domain,'fields':['us','uk','length','eu']}).then(function(result) {
                if (result){
                    $('#eu_size').val(result.eu);
                    $('#uk_size').val(result.uk);
                    $('#us_size').val(result.us);
                    $('#centimeters').val(result.length);
                }
            });
        });

    });

    $('.cls_eu_size').on('click', function(e){
        $('.cls_eu_size_ul').show();
        $('.eu_active_color').css('color','red');
        $('.uk_active_color').css('color','');
        $('.us_active_color').css('color','');
        $('.cls_uk_size_ul').css('display','none');
        $('.cls_us_size_ul').css('display','none');
    });

    $('.cls_uk_size').on('click', function(e){
        $('.cls_uk_size_ul').show();
        $('.uk_active_color').css('color','red');
        $('.eu_active_color').css('color','');
        $('.us_active_color').css('color','');
        $('.cls_eu_size_ul').css('display','none');
        $('.cls_us_size_ul').css('display','none');
    });

    $('.cls_us_size').on('click', function(e){
        $('.cls_us_size_ul').show();
        $('.us_active_color').css('color','red');
        $('.eu_active_color').css('color','');
        $('.uk_active_color').css('color','');
        $('.cls_uk_size_ul').css('display','none');
        $('.cls_eu_size_ul').css('display','none');

    });
    $('#filter_button').click(function () {
    	//$('.filter_categ_inner').animate({'display': 'block'}, 2500);
    	$(".filter_categ_inner").css('display','block');
    	$('.filter_categ_inner').hide();
    	$(".filter_categ_inner").animate({
    	            height: 'toggle',
    	            opacity: '0.9'
    	        },"fast");

    	$('#filter_button').hide();
    	});
//    	$( window ).scroll(function() {
        $('#filter_closed').click(function () {
    	        $('#filter_button').show();
    	        $('.filter_categ_inner').hide();
    	        $(".filter_categ_inner").css('display','none');
    	         $('.options').fadeOut(250, function () {
    	         $('.filter_categ_inner').removeClass('expand');
    	       });

    	});
//    $("#ex1").on("swipeleft",function(){
//      $(".prev").click();
//    });
//    $("#ex1").on("swiperight",function(){
//      $(".next").click();
//    });

});