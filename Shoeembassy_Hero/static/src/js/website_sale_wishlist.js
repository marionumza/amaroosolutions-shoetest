$( document ).ready(function() {
    odoo.define('Shoeembassy_Hero.wishlist', function (require) {
    "use strict";

    var ajax = require('web.ajax');
    var core = require('web.core');
    var Model = require('web.Model');
    var Widget = require('web.Widget');
    var base = require('web_editor.base');
    var website_sale_utils = require('web.utils');
    var session = require('web.session');

        if(!$('.oe_website_sale').length) {
            return $.Deferred().reject("DOM doesn't contain '.oe_website_sale'");
        }
        var self = this;
        var wishlist_product_ids = [];
        $('span.o_add_wishlist').on('click', function(e) {
            var data = $(this)
            var prod_id = $(this).data('product-data-id');
            var currentTargetnew = $(this).find('i');
            if ($(this).find('i').hasClass('fa fa-heart-o')){
                ajax.jsonRpc("/wishlist/user", 'call', {
                }).then(function (result){
                    if(result){
                        add_new_products(data, e);
                        var products = $($.find('span[product-data-id='+prod_id+']>i'))
                            
                        currentTargetnew.removeClass('fa-heart-o').addClass('fa-heart')
                        return;
                    }
                    else{
                        window.location.href = '/web/login'
                    }
                });
            }

            var currentTargetnew = $(this).find('i');
            if ($(this).find('i').hasClass('fa fa-heart')){
                var prod_id = $(this).data('product-data-id');
                new Model('product.wishlist').call('search_read', [[]], {'fields': ['product_id', 'user_id']}).then(function(datas) {
                    for (var i = 0; i < datas.length; i++) {
                        var prod_temp_id = datas[i]['product_id'][0];
                        var usr_id = datas[i]['user_id'][0];
                        if (prod_temp_id == prod_id) {
                            var del_id = datas[i]['id'];
                            new Model('product.wishlist').call('unlink', [[del_id]]);
                            wishlist_product_ids.pop(del_id)
                            var products = $($.find('span[product-data-id='+prod_id+']>i'))
                            currentTargetnew.removeClass('fa-heart').addClass('fa-heart-o')
                            return;
                        }
                    }

                });
            }
            return; 
        });
        function add_new_products($el, e){
            var self = this;

            var product_id = $el.data('product-data-id');
            console.log(product_id);
            if (odoo.session_info.is_website_user){
                warning_not_logged();
            } else {
                return ajax.jsonRpc('/shop/wishlist/add', 'call', {
                    'product_id': product_id,
                }).then(function () {
                    wishlist_product_ids.push(product_id);
                    update_wishlist_view();
                });
            }
            function display_wishlist() {
                if (odoo.session_info.is_website_user) {
                    warning_not_logged();
                }
                else if (wishlist_product_ids.length = 0) {
                    update_wishlist_view();
                    redirect_no_wish();
                }
                else {           
                    window.location = '/shop/wishlist';
                }
                function redirect_no_wish() {
                    window.location = '/'
                }
                function warning_not_logged() {
                    window.location = '/shop/wishlist';
                }
            }
        }
        function update_wishlist_view() {
            if (wishlist_product_ids.length > 0) {
                $('#my_wish').show();
                $('.my_wish_quantity').text(wishlist_product_ids.length);
            }
            else {
                $('#my_wish').show();
            }
        }
        function wishlist_rm(e){
            var tr = $(e.currentTarget).parents('tr');
            var wish = tr.data('wish-id');
            var product = tr.data('product-id');
            new Model('product.wishlist')
                .call('write', [[wish], { active: false }, base.get_context()])
                .then(function(){
                    $(tr).remove();
                    if ($('.table-responsive tbody tr').length < 1) {
                        setTimeout(window.location = '/shop', 5000);
                    }
                });
        }

        function wishlist_add(e){
            var tr = $(e.currentTarget).parents('tr');
            var product = tr.data('product-id');
            wishlist_rm(e);
            add_to_cart(product, tr.find('qty').val() || 1);
        }

        if ($('.wishlist-section').length) {
            $('.wishlist-section a.o_wish_rm').on('click', function (e){ wishlist_rm(e); });
            $('.wishlist-section a.o_wish_add').on('click', function (e){ wishlist_add(e); });
        }

        function add_to_cart(product_id, qty_id) {
            ajax.jsonRpc("/shop/cart/update_json", 'call', {
                'product_id': parseInt(product_id, 10),
                'add_qty': parseInt(qty_id, 10)
            }).then(function (data) {

                var $q = $(".my_cart_quantity");

                console.log(data.quantity);
                if (data.cart_quantity) {
                    $q.parents('li:first').removeClass("hidden");
                }
                else {
                    $q.parents('li:first').addClass("hidden");
                    $('a[href^="/shop/checkout"]').addClass("hidden");
                }
                $q.html(data.cart_quantity).hide().fadeIn(600);
                $q.val(data.quantity);
            });
        }
    });
});