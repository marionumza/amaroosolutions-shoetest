odoo.define('Shoeembassy_Hero.hide_header', function (require) {
"use strict";
	$( document ).ready(function() {
	    var url = window.location.pathname;  
	    // if(url == '/shop/cart' || url == '/shop/checkout' || url=='/shop/extra_info' || url=='/shop/payment'){
	    // 	$('.top-header-menu').addClass("hidden");
	    // }
	    if(url == '/shop/cart'){
	    	$('.menu-categ').addClass("hidden");
	    	$('#top_menu').append('<li class="single_categ" id="review"><a href="/shop/category/" name="Review Order">Review Order</a></li>');
	    	$('#td-price span').css("margin-left","25px");
	    	$('#td-price .oe_currency_value').css("margin-left","0px");
	    }
	    else if(url == '/shop/checkout'){
    		$('.menu-categ').addClass("hidden");
	    	$('#top_menu').append('<li class="single_categ" id="shipping"><a href="/shop/category/" name="Shipping & Billing">Shipping & Billing</a></li>');
	    }
	    else if(url=='/shop/extra_info'){
	    	$('.top-header-menu').addClass("hidden");
	    	$('.menu-categ').removeClass("hidden");
	    }
	    else if(url=='/shop/payment'){
	    	$('.menu-categ').addClass("hidden");
	    	$('#top_menu').append('<li class="single_categ" id="payment_menu"><a href="/shop/payment/" name="Payment">Payment</a></li>');
	    }
	    // else if(url=='/shop/confirmation'){
	    	/*$('.top-header-menu').addClass("hidden");*/
	    	/*$('.menu-categ').removeClass("hidden");*/
	    /*}*/
	    else{
	    	$('#top_menu').remove('#shipping','#review');
	    	$('.menu-categ').removeClass("hidden");
	    	$('.top-header-menu').removeClass("hidden");	
	    }
	});
	
	
});