# -*- coding: utf-8 -*-
{
    'name': "Shoeembassy Hero",
    'summary': """
        """,
    'description': """
    """,
    'author': "MPTechnolabs",
    'website': "",
    'category': 'Website',
    'version': '0.1',
    'depends': ['base', 'stock', 'website_sale', 'website', 'auth_signup', 'im_livechat','mail','shoe_embassy_app','website_crm'],
    'data': [
    	'security/ir.model.access.csv',
        'views/snippets.xml',
        'view/warehouse_details_view.xml',
        'view/our_offers_view.xml',
        'view/product_category_view.xml',
        'view/footer_footer_view.xml',
        'view/product_template_view.xml',
        'view/website_menu_view.xml',
        'view/product_attribute_view.xml',
        'views/homepage_view.xml',
        'views/assets.xml',
        'views/res_config_settings_view.xml',
        'views/category_page_view.xml',
        'views/website_menu_data.xml',
        'views/wishlist_product_view.xml',
        'views/wish_list_view.xml',
        'views/website_crm_templates.xml',
        'view/alternative_product_update.xml',
        'view/update_product_image.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
