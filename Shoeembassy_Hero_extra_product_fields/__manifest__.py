# -*- coding: utf-8 -*-
{
    'name': "Shoeembassy Hero Extra Product Fields.",
    'summary': """
        """,
    'description': """
    """,
    'author': "MPTechnolabs",
    'website': "",
    'category': 'Website',
    'version': '0.1',
    'depends': ['base', 'product'],
    'data': [
        'security/ir.model.access.csv',
        'view/product_template_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
