# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ShoeEmbassyBrand(models.Model):
    _name = 'shoe.embassy.brand'
    _order = 'name'

    name = fields.Char(string='Brand')


class ShoeEmbassyColour(models.Model):
    _name = 'shoe.embassy.colour'

    name = fields.Char(string='Colour')


class  ProductTemplate(models.Model):
    
    _inherit = 'product.template'
    
    brand_id = fields.Many2one('shoe.embassy.brand', string='Brand Name')
    colour_id = fields.Many2one('shoe.embassy.colour', string='Colour')


