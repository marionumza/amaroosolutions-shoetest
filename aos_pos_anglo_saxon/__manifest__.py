# -*- coding: utf-8 -*-
{
    "name": "POS Anglo-Saxon",
    "version": '10.0.1.0.0',
    'summary': 'Point of Sale Anglo Saxon Company',
    'author': 'Alphasoft',
    'website': 'http://www.alphasoft.co.id',
    "description": """
        Module for anglosaxon point of sale
    """,
    'category' : 'Point of Sale',
    "depends": ['point_of_sale'],
    'images' : ['static/description/main_screenshot.png'],
    'init_xml': [],
    'data': [
    ],
    'css': [],
    'js': [
    ],
    'price': 55.00,
    'currency': 'EUR',
    'installable': True,
    'active': False,
    'application': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
