# -*- coding: utf-8 -*-
import time

from odoo.osv import expression
from datetime import date, datetime
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.modules import get_module_resource
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, image_colorize,\
    image_resize_image_big
from odoo.exceptions import except_orm, Warning as UserError

class PosOrder(models.Model):
    _inherit = "pos.order"
    
    def _create_account_move_line(self, session=None, move=None):
        # Tricky, via the workflow, we only have one id in the ids variable
        """Create a account move line of order grouped by products or not."""
        IrProperty = self.env['ir.property']
        ResPartner = self.env['res.partner']

        if session and not all(session.id == order.session_id.id for order in self):
            raise UserError(_('Selected orders do not have the same session!'))

        grouped_data = {}
        have_to_group_by = session and session.config_id.group_by or False
        #print "==_create_account_move_line==",order
        for order in self.filtered(lambda o: not o.account_move or self.state == 'paid'):
        #for order in self.filtered(lambda o: not o.account_move or order.state == 'paid'):
            current_company = order.sale_journal.company_id
            account_def = IrProperty.get(
                'property_account_receivable_id', 'res.partner')
            order_account = order.partner_id.property_account_receivable_id.id or account_def and account_def.id
            partner_id = ResPartner._find_accounting_partner(order.partner_id).id or False
            if move is None:
                # Create an entry for the sale
                journal_id = self.env['ir.config_parameter'].sudo().get_param(
                    'pos.closing.journal_id_%s' % current_company.id, default=order.sale_journal.id)
                move = self._create_account_move(
                    order.session_id.start_at, order.name, int(journal_id), order.company_id.id)

            def insert_data(data_type, values):
                # if have_to_group_by:
                values.update({
                    'partner_id': partner_id,
                    'move_id': move.id,
                })

                if data_type == 'product':
                    key = ('product', values['partner_id'], (values['product_id'], tuple(values['tax_ids'][0][2]), values['name']), values['analytic_account_id'], values['debit'] > 0)
                #===============add cogs for anglo saxon========================
                elif data_type == 'anglo_saxon_product_debit':
                    key = ('anglo_saxon_product_debit', values['partner_id'], (values['product_id'], values['name']), values['analytic_account_id'], values['debit'] > 0)
                elif data_type == 'anglo_saxon_product_credit':
                    key = ('anglo_saxon_product_credit', values['partner_id'], (values['product_id'], values['name']), values['analytic_account_id'], values['credit'] > 0)
                #===============================================================
                elif data_type == 'tax':
                    key = ('tax', values['partner_id'], values['tax_line_id'], values['debit'] > 0)
                elif data_type == 'counter_part':
                    key = ('counter_part', values['partner_id'], values['account_id'], values['debit'] > 0)
                else:
                    return

                grouped_data.setdefault(key, [])

                if have_to_group_by:
                    if not grouped_data[key]:
                        grouped_data[key].append(values)
                    else:
                        current_value = grouped_data[key][0]
                        current_value['quantity'] = current_value.get('quantity', 0.0) + values.get('quantity', 0.0)
                        current_value['credit'] = current_value.get('credit', 0.0) + values.get('credit', 0.0)
                        current_value['debit'] = current_value.get('debit', 0.0) + values.get('debit', 0.0)
                else:
                    grouped_data[key].append(values)

            # because of the weird way the pos order is written, we need to make sure there is at least one line,
            # because just after the 'for' loop there are references to 'line' and 'income_account' variables (that
            # are set inside the for loop)
            # TOFIX: a deep refactoring of this method (and class!) is needed
            # in order to get rid of this stupid hack
            assert order.lines, _('The POS order must have lines when calling this method')
            # Create an move for each order line
            cur = order.pricelist_id.currency_id
            for line in order.lines:
                amount = line.price_subtotal
                # Search for the income account
                if line.product_id.property_account_income_id.id:
                    income_account = line.product_id.property_account_income_id.id
                elif line.product_id.categ_id.property_account_income_categ_id.id:
                    income_account = line.product_id.categ_id.property_account_income_categ_id.id
                else:
                    raise UserError(_('Please define income '
                                      'account for this product: "%s" (id:%d).')
                                    % (line.product_id.name, line.product_id.id))

                name = line.product_id.name
                if line.notice:
                    # add discount reason in move
                    name = name + ' (' + line.notice + ')'

                # Create a move for the line for the order line
                insert_data('product', {
                    'name': name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': income_account,#((amount > 0) and amount) and income_account or ((amount < 0) and -amount) and return_income_account,
                    'analytic_account_id': self._prepare_analytic_account(line),
                    'credit': ((amount > 0) and amount) or 0.0,
                    'debit': ((amount < 0) and -amount) or 0.0,
                    'tax_ids': [(6, 0, line.tax_ids_after_fiscal_position.ids)],
                    'partner_id': partner_id
                })
                #===============================================================
                if not order.company_id.anglo_saxon_accounting:
                    raise UserError(_('Please checklist Use Anglo-Saxon Accounting * on Invoicing Settings first!'))
                fpos = line.order_id.fiscal_position_id
                amount_cogs_unit = ((amount > 0) and (line._get_anglo_saxon_price_unit())) or ((amount < 0) and -(line._get_anglo_saxon_price_unit())) or 0.0
                amount_cogs_subtotal = amount_cogs_unit*line.qty
                #print "===amount===",amount_cogs,line.price_subtotal
                accounts = line.product_id.product_tmpl_id.get_product_accounts(fiscal_pos=fpos)
                # debit account dacc will be the output account
                if amount_cogs_subtotal >= 0:
                    #if sales
                    dacc = accounts['stock_output'].id #HPP
                    cacc = accounts['expense'].id #PERSEDIAAN
                    # credit account cacc will be the expense account
                elif amount_cogs_subtotal < 0:
                    #if return
                    dacc = accounts['expense'].id #PERSEDIAAN
                    cacc = accounts['stock_output'].id #HPP
                # Create a move cogs debit for the line for the order line
                insert_data('anglo_saxon_product_debit', {
                    'name': name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': dacc,
                    'analytic_account_id': self._prepare_analytic_account(line),
                    'credit': ((amount_cogs_subtotal < 0) and -amount_cogs_subtotal) or 0.0,
                    'debit': ((amount_cogs_subtotal > 0) and amount_cogs_subtotal) or 0.0,
                    'partner_id': partner_id
                })
                # Create a move cogs debit for the line for the order line
                insert_data('anglo_saxon_product_credit', {
                    'name': name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': cacc,
                    'analytic_account_id': self._prepare_analytic_account(line),
                    'credit': ((amount_cogs_subtotal > 0) and amount_cogs_subtotal) or 0.0,
                    'debit': ((amount_cogs_subtotal < 0) and -amount_cogs_subtotal) or 0.0,
                    'partner_id': partner_id
                })
                #===============================================================
                # Create the tax lines
                taxes = line.tax_ids_after_fiscal_position.filtered(lambda t: t.company_id.id == current_company.id)
                if not taxes:
                    continue
                for tax in taxes.compute_all(line.price_unit * (100.0 - line.discount) / 100.0, cur, line.qty)['taxes']:
                    insert_data('tax', {
                        'name': _('Tax') + ' ' + tax['name'],
                        'product_id': line.product_id.id,
                        'quantity': line.qty,
                        'account_id': tax['account_id'] or income_account,
                        'credit': ((tax['amount'] > 0) and tax['amount']) or 0.0,
                        'debit': ((tax['amount'] < 0) and -tax['amount']) or 0.0,
                        'tax_line_id': tax['id'],
                        'partner_id': partner_id
                    })

            # counterpart
            insert_data('counter_part', {
                'name': _("Trade Receivables"),  # order.name,
                'account_id': order_account,
                'credit': ((order.amount_total < 0) and -order.amount_total) or 0.0,
                'debit': ((order.amount_total > 0) and order.amount_total) or 0.0,
                'partner_id': partner_id
            })

            order.write({'state': 'done', 'account_move': move.id})
        all_lines = []
        for group_key, group_data in grouped_data.iteritems():
            for value in group_data:
                all_lines.append((0, 0, value),)
        if move:  # In case no order was changed
            move.sudo().write({'line_ids': all_lines})
            move.sudo().post()
        return True
    
class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'
    
    def _get_anglo_saxon_price_unit(self):
        self.ensure_one()
        price = self.product_id.standard_price
        return price
    
    def _get_price(self, company_currency, price_unit):
        if self.order_id.pricelist_id.currency_id.id != company_currency.id:
            price = company_currency.with_context(date=self.order_id.date_order).compute(price_unit * self.qty, self.order_id.pricelist_id.currency_id)
        else:
            price = price_unit * self.qty
        return round(price, self.order_id.pricelist_id.currency_id.decimal_places)

         