# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# License  URL :<https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Multi Channel  Magento 1.X Odoo Bridge(Multi Channel-MOB)",
  "summary"              :  "Magento 1.x Multi Channel Odoo extension provides in-depth integration with Odoo and Magento 1.x series.",
  "category"             :  "Website",
  "version"              :  "0.1.1",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "license"              :  "Other proprietary",
  "website"              :  "https://store.webkul.com/Multi-Channel-Magento-1-x-Odoo-Bridge-Multi-Channel-MOB.html",
  "description"          :  """Magento1.x Odoo Bridge  is the  Odoo Extension to connect Magento and Odoo.
                                it import order , partner , product , category in odoo from Magento 1 series.
                                it also manage product stock / Inventory and  update order status over magento end for creating shipment / delivery and invoice.
                                This module also support feature of product export/update and product image export/update   over mangeto .""",
  "live_test_url"        :  "http://odoo.webkul.com:8010/web?db=Magento1x_Odoo_Bridge",
  "depends"              :  ['odoo_multi_channel_sale'],
  "data"                 :  [
                             'security/ir.model.access.csv',
                             'data/cron.xml',
                             'data/data.xml',
                             'views/dashboard.xml',
                             'wizard/wizard.xml',
                             'wizard/inherits.xml',
                             'views/views.xml',
                             'views/search.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  False,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  100,
  "currency"             :  "EUR",
}
