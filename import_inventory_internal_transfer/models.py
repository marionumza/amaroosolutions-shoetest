# -*- coding: utf-8 -*-
from odoo import models, fields, api
from xlrd import open_workbook


class ImportFile(models.Model):
    _name = 'stock.inventory.moves.file'

    xlsx_file = fields.Binary(string="File", attachment=True)

    @api.multi
    def import_init(self, context=None):
        import codecs
        # Get the model form picking form
        stock_model = self.env['stock.picking'].search([('id', '=', context.get('stock_id'))])
        product_model = self.env['product.product']

        excel_file = codecs.decode(self.xlsx_file, 'base64')
        wb = open_workbook(file_contents=excel_file)
        data_sheets = []

        # Parse all data from Excel file
        for s in wb.sheets():
            data_rows = []
            for row_key, row in enumerate(range(s.nrows)):
                if (row_key != 0):
                    data_row = []
                    for index, col in enumerate(range(s.ncols)):
                        value = (s.cell(row, col).value)
                        if (isinstance(value, float)):
                            value = int(value)
                        data_row.append(value)
                    data_rows.append(data_row)
            data_sheets.append(data_rows)
        for sheet in data_sheets:
            for row in sheet:
                product = product_model.search([('default_code', '=', str(row[0])), ('company_id', '=', self.env.user.company_id.id)])
                if (product):
                    stock_model.write({
                        'move_lines': [(0, 0, {
                            'name': product.name,
                            'product_id': product.id,
                            'product_uom': product.uom_id.id,
                            'product_uom_qty': row[1],
                            'location_id': context.get('location_id'),
                            'location_dest_id': context.get('location_dest_id')
                        })]
                    })

        return


class Stock(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def show_import_form(self):
        xml_record = self.env.ref("import_inventory_internal_transfer.import_lines_form")
        result = {
            'name': "Import product lines",
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': "stock.inventory.moves.file",
            'context': {
                'stock_id': self.id,
                'location_id': self.location_id.id,
                'location_dest_id': self.location_dest_id.id,
                'picking_type_id': self.picking_type_id.id},
            'type': 'ir.actions.act_window',
            'view_id': [xml_record.id],
            'target': 'new'
        }
        return result
