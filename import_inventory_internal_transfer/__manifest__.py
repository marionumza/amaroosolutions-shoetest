# -*- coding: utf-8 -*-

{
    "name": "Import inventory internal transfer from Excel",
    "summary": "Fastly import alot of stock move lines in Internal Transfer",
    "version": "1.0.0",
    "category": "Inventory",
    "website": "http://www.dtn.com.vn",
    "author": "SmallMaster",
    "license": "AGPL-3",
    "application": True,
    "installable": True,
    "images": ['static/description/cover.png'],
    "depends": [
        "base",
        "stock",
    ],
    'data': [
        'views/view_data.xml'
    ],
    'price': 25,
    'currency': 'EUR'
}
