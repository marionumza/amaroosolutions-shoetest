=============================================================================
Import product move lines in Internal Transfer from Excel (.xls, .xlsx) files
=============================================================================

Create new **Internal Transfer**

.. image:: https://i.imgur.com/R4r5Aaj.png?

Click on **Import from Excel** button

.. image:: https://i.imgur.com/3nYXSKm.png?

This is the content of Excel files. It must contains **product code** and **product quantity**

.. image:: https://i.imgur.com/pd77pjw.png?

Product code is **Internal Reference** field in product form

.. image:: https://i.imgur.com/G7kqVKD.png?

Click **Apply** button and product move lines will be inserted into the form

.. image:: https://i.imgur.com/Q2o5tGX.png
