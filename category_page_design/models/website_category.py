# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _
from odoo.exceptions import  ValidationError

class ProductPublicCategory(models.Model):

    _inherit = 'product.public.category'

    banner_label = fields.Char(string='Banner Label')
    opacity = fields.Selection(string="Banner opacity", selection=[
        ('0.1', 'Invisible'),
        ('0.3', '30% visibility'),
        ('0.6', '60% visibility'),
        ('0.8', '80% visibility'),
        ('0.9', '90% visibility'),
        ('1', '100% visibility')])

    text_color = fields.Char('Banner font Color', help="Here you can set a "
    "specific HTML color index (e.g. #ff0000) to set the font color of sub category.")
    rgba_color = fields.Char(string="RGBA", help="rgb color code And the 'a' for the opacity 0.1 to 1(225,255,225,0.4)")
    text_size_banner = fields.Integer(string="Banner font size", help="banner font size in px")
    parallax_layer = fields.Char(string="Parallax layer", help="rgb color code And the 'a' for the opacity 0.1 to 1(225,255,225,0.4)")

    # @api.multi
    # @api.constrains('is_search_categ')
    # def _check_search_categ(self):
    #     if len(self.search([]).filtered(lambda c: c.is_search_categ))>1:
    #         raise ValidationError(_('There can be only 1 search category'))
    #
    # @api.multi
    # def get_size_correlation(self):
    #     gender_name = self.name.lower()
    #     men_gender_id = self.env['gender'].sudo().search([('name','=','Mens')])
    #     women_gender_id = self.env['gender'].sudo().search([('name','=','Womens')])
    #     vals = {}
    #     if self.attribute_value_ids:
    #         gender_id = False
    #         if women_gender_id.name.startswith('Womens'):
    #             gender_id = women_gender_id
    #         if women_gender_id.name.startswith('Mens'):
    #             gender_id = men_gender_id
    #         if gender_id:
    #             for val in self.attribute_value_ids:
    #                 size_id = self.env['size.correlation'].sudo().search([('eu','=',val.name),('gender_id','=',gender_id.id)])
    #                 if size_id:
    #                     vals.update({val.id:size_id.uk})
    #     return vals