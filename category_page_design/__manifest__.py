{
    'name': 'category_page',
    'version': '0.3',
    'author': 'jithesh',
    'category': 'www.socius.com',
    'summary': 'Adding changes to current modules',
    'description': """This module created for to change the colour and dynamic changes for current websit """,
    'images': [
    ],
    'depends': ['base','web','Shoeembassy_Hero','website_sale'],
    'data': [
        'views/assets_websites.xml',
        'views/web_views.xml',
        'views/website_mobile_banner_header.xml',
        'views/product_page.xml',
        'views/extra.xml',
        ],
    'installable': True
}
