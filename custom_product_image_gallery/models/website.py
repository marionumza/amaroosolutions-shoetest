# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _


class Website(models.Model):
    _inherit = 'website'

    thumbnail_panel_position = fields.Selection([
        ('left', 'Left'),
        ('right', 'Right'),
        ('bottom', 'Bottom'),
    ], default='left',
        string='Thumbnails panel position',
        help="Select the position where you want to display the thumbnail panel in multi image.")
    interval_play = fields.Char(
        string='Play interval of slideshow',
        default='5000',
        help='With this field you can set the interval play time between two images.')
    enable_disable_text = fields.Boolean(
        string='Enable the text panel',
        default=True,
        help='Enable/Disable text which is visible on the image in multi image.')
    color_opt_thumbnail = fields.Selection([
        ('default', 'Default'),
        ('b_n_w', 'B/W'),
        ('sepia', 'Sepia'),
        ('blur', 'Blur'), ],
        default='default',
        string="Thumbnail overlay effects")
    no_extra_options = fields.Boolean(
        string='Slider effects',
        default=True,
        help="Slider with all options for next, previous, play, pause, fullscreen, hide/show thumbnail panel.")
    change_thumbnail_size = fields.Boolean(string="Change thumbnail size", default=False)
    thumb_height = fields.Char(string='Thumb height', default=50)
    thumb_width = fields.Char(string='Thumb width', default=88)
    group_website_multiimage = fields.Selection([
        (0, 'One image per product'),
        (1, 'Several images per product'),
        (2, 'Custom Image Gallery'), ],
        string='Multi Images',
        implied_group='website_sale.group_website_multi_image',
        group='base.group_portal,base.group_user,base.group_public')

    @api.multi
    def get_multiple_images(self, product_id=None):
        product_img_data = False
        if product_id:
            self.env.cr.execute(
                "select id from biztech_product_images where product_tmpl_id=%s and more_view_exclude IS NOT TRUE order by sequence", ([product_id]))
            product_ids = map(lambda x: x[0], self.env.cr.fetchall())
            if product_ids:
                product_img_data = self.env['biztech.product.images'].browse(
                    product_ids)
        return product_img_data
    
    @api.multi
    def get_galleries_len(self, product_id=None):
        galleries_len = False
        if product_id:
            for record in self.env['product.template'].sudo().browse(product_id):
                if record.multi_image and record.images:
                     galleries_len = galleries_len + 1
                if record.product_variant_image_ids:
                    galleries_len = galleries_len + len(record.product_variant_image_ids.ids)
        if galleries_len:
            return galleries_len
        else:
            return 0

    @api.multi
    def get_variant_image(self, product_id=None):
        print "method called"
        variant_images = False
        if product_id:
            self.env.cr.execute(
                "select id from shoe_product_variant_image where attribute_value_id=%s order by sequence", ([product_id]))
            product_ids = map(lambda x: x[0], self.env.cr.fetchall())
            if product_ids:
                variant_images = self.env['shoe.product.variant.image'].browse(product_ids)
        return variant_images
                


class WebsiteConfigSettings(models.TransientModel):

    _inherit = 'website.config.settings'

    thumbnail_panel_position = fields.Selection([
        ('left', 'Left'),
        ('right', 'Right'),
        ('bottom', 'Bottom')],
        string='Thumbnails panel position',
        related='website_id.thumbnail_panel_position',
        help="Select the position where you want to display the thumbnail panel in multi image.")
    interval_play = fields.Char(
        string='Play interval of slideshow',
        related='website_id.interval_play',
        help='With this field you can set the interval play time between two images.')
    enable_disable_text = fields.Boolean(
        string='Enable the text panel',
        related='website_id.enable_disable_text',
        help='Enable/Disable text which is visible on the image in multi image.')
    color_opt_thumbnail = fields.Selection([
        ('default', 'Default'),
        ('b_n_w', 'B/W'),
        ('sepia', 'Sepia'),
        ('blur', 'Blur')],
        related='website_id.color_opt_thumbnail',
        string="Thumbnail overlay effects")
    no_extra_options = fields.Boolean(
        string='Slider effects',
        # default=True,
        related='website_id.no_extra_options',
        help="Slider with all options for next, previous, play, pause, fullscreen, hide/show thumbnail panel.")
    change_thumbnail_size = fields.Boolean(
        string="Change thumbnail size",
        related="website_id.change_thumbnail_size")
    thumb_height = fields.Char(
        string='Thumb height',
        related="website_id.thumb_height")
    thumb_width = fields.Char(
        string='Thumb width',
        related="website_id.thumb_width")
    group_website_multiimage = fields.Selection([
        (0, 'One image per product'),
        (1, 'Several images per product'),
        (2, 'Custom Image Gallery'), ],
        string='Multi Images',
        related='website_id.group_website_multiimage',
        implied_group='website_sale.group_website_multi_image',
        group='base.group_portal,base.group_user,base.group_public')
