# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _


class ProductImages(models.Model):
    _name = 'biztech.product.images'
    _description = "Add Multiple Image in Product"

    name = fields.Char(string='Title', translate=True)
    alt = fields.Char(string='Alt', translate=True)
    attach_type = fields.Selection([('image', 'Image'),('video', 'Video')],
        default='image',
        string="Type")
    image = fields.Binary(string='Image')
    video_type = fields.Selection([('youtube', 'Youtube'),
                                   ('vimeo', 'Vimeo'),
                                   ('html5video','Html5 Video')],
        default='youtube',
        string="Video media player")
    cover_image = fields.Binary(string='Cover image',
        # required=True,
        help="Cover Image will be show untill video is loaded.")
    video_id = fields.Char(string='Video ID')
    video_ogv = fields.Char(string='Video OGV', help="Link for ogv format video")
    video_webm = fields.Char(string='Video WEBM', help="Link for webm format video")
    video_mp4 = fields.Char(string='Video MP4', help="Link for mp4 format video")
    sequence = fields.Integer(string='Sort Order')
    product_tmpl_id = fields.Many2one('product.template', string='Product')
    more_view_exclude = fields.Boolean(string="More View Exclude")
    variant_image_id = fields.Many2one('shoe.product.variant.image', 'Variant Image ID')
    
class ProductVariantImages(models.Model):
    _name = 'shoe.product.variant.image'
    _description = 'Shoe Variant Image'
    
    attribute_id = fields.Many2one('product.attribute', 'Attribute Name')
    attribute_value_id = fields.Many2one('product.attribute.value', 'Value')
    variant_image = fields.Binary(string='Image')
    product_tmplate_id = fields.Many2one('product.template', string='Product')
    sequence = fields.Integer(string='Sequence')
    images = fields.One2many('biztech.product.images', 'variant_image_id',
                              string='Images')
    

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    _description = "Multiple Images"

    images = fields.One2many('biztech.product.images', 'product_tmpl_id',
                              string='Images')
    multi_image = fields.Boolean(string="Add Multiple Images?")
    
    product_variant_image_ids = fields.One2many('shoe.product.variant.image', 'product_tmplate_id')
