========================
``Custom Product Image Gallery`` changelog
========================

******************************************
Fixed issues
******************************************

1. Fixed an issue of duplicate images when 'Add Multi Images' was marked true and Show single options was ticked in website admin.
2. Added translation folder i18n for french[BE].

******************************************
Fixed issues
******************************************

 1/. Fixed issue for image which is not respecting responsiveness. 
