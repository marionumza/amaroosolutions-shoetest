# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

{
    'name': 'Custom Product Image Gallery',
    'description': '''Website Product Feature
Product picture
Multiple Images Per Product
Website Product Multi Images
Website Product Images Gallery
Website Multi Images
Website gallery
Product gallery
Product Multi Image
Product Multi Images
Product Zoom''',
    'summary': 'Product Image Gallery Comes With Advanced Specs',
    'category': 'Website Sale',
    'version': '10.0.1.0.2',
    'author': 'AppJetty',
    'website': 'https://goo.gl/IBH9qG',
    'depends': ['website_sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/product_view.xml',
        'views/product_template.xml',
        'views/website_config_view.xml',
    ],
    'images': ['static/description/splash-screen.png'],
    'price': 19.00,
    'currency': 'EUR',
    'application': True,
}
