
{
    'name': "custom_website",
    'summary': """This Module is used for easy to Add product in cart""" ,
    'version': '11.0.0.0',
    'category': 'eCommerce',
    'author': 'Jithesh m.p',
    'description': 'This Module is used to Add product in cart, easy to Add product in cart, dynamic add to cart option, dynamic product add to cart option, easy add to cart option on shop',
    'license':'OPL-1',
    'depends': ['base','website','Shoeembassy_Hero','website_sale'],

    'data'   : [
                'views/modals.xml',
                'views/website_data.xml',
                'views/viewss.xml',
    		   ],
	'installable': True,
	'application': True,

}